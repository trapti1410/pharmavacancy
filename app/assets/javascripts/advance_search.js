$(document).ready( function () {
    $('.js-adv_search').on('click', function (event) {
        event.preventDefault();
        if (validateAdvanceSearch()) {
            $('.js-adv_search_form').submit();
        }

    });
});

    function validateAdvanceSearch() {
    	var returnValue = true;

    	var keySkills =  $('.js-key_skills');
    	var location  =  $('.js-location');
    	var category  =  $('.js-category');
        var minExpSalary = $('.js-min_exp_salary');
        var maxExpSalary = $('.js-max_exp_salary');

    	if (!isEmpty(keySkills)) {
            returnValue = false;
        }
        if (!isEmpty(location)) {
            returnValue = false;
        }
        if (!isEmpty(category)) {
            returnValue = false;
        }
        if ( !isGreater(minExpSalary,maxExpSalary) ) {
            returnValue = false;
        }

    	return returnValue;
    }