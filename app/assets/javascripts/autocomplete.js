$(function() {

    // Below is the name of the textfield that will be autocomplete    
    $('#keywords').autocomplete({
            minLength: 2,
            source: '/ajax/keywords'
    });

    $('#search_location').autocomplete({
            minLength: 2,
            source: '/ajax/locations'
    });

    // Below is the name of the textfield that will be autocomplete    
    $('#adv_keywords').autocomplete({
            minLength: 2,
            source: '/ajax/keywords'
    });

    $('#adv_search_location').autocomplete({
            minLength: 2,
            source: '/ajax/locations'
    });

    // Below is the name of the textfield that will be autocomplete    
    $('#alert_keywords').autocomplete({
            minLength: 2,
            source: '/ajax/keywords'
    });

    $('#alert_location').autocomplete({
            minLength: 2,
            source: '/ajax/locations'
    });

    
});