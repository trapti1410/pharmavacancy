$(document).ready( function () {
    $('.js-candidate_step1_update_submit').on('click', function (event) {
        event.preventDefault();
        if (validateSnapshot()) {
            $('.js-candidate_edit_profile_snapshot').submit();
        }

    });

    $('.js-candidate_step2_update_submit').on('click', function (event) {
        event.preventDefault();
        if (validateProfileSummary()) {
            $('.js-candidate_edit_profile_summary').submit();
        }
    });

    $('.js-candidate_step3_update_submit').on('click', function (event) {
        event.preventDefault();
        if (validateEmployementDetails()) {
            $('.js-candidate_edit_employment_details').submit();
        }
    });

    $('.js-candidate_step4_update_submit').on('click', function (event) {
        event.preventDefault();
        if (validateProjects()) {
            $('.js-candidate_edit_project').submit();
        }
    });

    $('.js-candidate_step5_update_submit').on('click', function (event) {
        event.preventDefault();
        if (validateSkills()) {
            $('.js-candidate_edit_skill').submit();
        }
        
    });

    $('.js-candidate_step6_update_submit').on('click', function (event) {
        event.preventDefault();
        $('.js-candidate_edit_education').submit();
    });

    $('.js-candidate_step7_update_submit').on('click', function (event) {
        event.preventDefault();
        $('.js-candidate_edit_other').submit();
    });

    $('.js-candidate_step8_update_submit').on('click', function (event) {
        event.preventDefault();
        if (validateResume()) {

            $('.js-candidate_edit_resume').submit();
    }

    });

    $('.js-candidate_exp_locations').on('change', function (event) {
        event.preventDefault();

        if ($(this).val().length > 0) {
            fetch_and_show_cities($(this).val(), 'candidate_exp');   
        } else {
            $('.js-cities').html('');
        }
    });

    $('.search-drop').mouseover(function(){
        $("."+$(this).attr('data-value')).stop().css({'z-index':'100'}).slideDown('slow');
    });

    $('.main-line').mouseleave(function(){
        $("."+$(this).attr('data-value')).slideUp('slow');
    });


});

function validateSnapshot() {
    var returnValue = true;
    var currentCompany = $('.js-current_company');
    var skills = $('.js-skills');
    var annualSalary = $('.js-annual_salary');
    var experience = $ ('.js-experience');
    var phoneNumber = $('.js-phone_number');
    var email = $ ('.js-email');

    if (isEmpty(currentCompany) === false) {
        returnValue = false;
    }

    if (isEmpty(skills) === false) {
        returnValue = false;
    }

    if (isEmpty(annualSalary) === false) {
        returnValue = false;
    }

    if (isEmpty(experience) === false) {
        returnValue = false;
    }
    
    if (isEmpty(phoneNumber) === false) {
        returnValue = false;
    }

    if (isEmpty(email) === false) {
        returnValue = false;
    }

    return returnValue;

}

function validateProfileSummary() {
    var returnValue = true;
    var profileSummary = $('.js-profile_summary');                            

    returnValue = isEmpty(profileSummary);

    return returnValue;
}

function validateEmployementDetails() {
    var returnValue = true;
    var employerName = $('.js-employer_name');
    
    returnValue = isEmpty(employerName);

    return returnValue;
}

function validateProjects() {
    var returnValue = true;
    var clientName = $('.js-client_name');
    var title = $('.js-title'); 

    if (isEmpty(clientName) === false) {
        returnValue = false;
    }
    if (isEmpty(title) === false) {
        returnValue = false;
    }

    return returnValue;
}

function validateSkills() {
    var returnValue = true;
    
    var skillName = $('.js-skill_name');
    var experience = $('.js-experience'); 

    if (isEmpty(skillName) === false) {
        returnValue = false;
    }

    if (isEmpty(experience) === false) {
        returnValue = false;
    }

    return returnValue;
}

function validateResume() {
    var returnValue = true;

    var file = $('input[type="file"]').val();
    var exts = ['doc','docx','rtf','pdf'];

    if (file.length > 0) {
        var get_ext = file.split('.');
            get_ext = get_ext.reverse();
        if ($.inArray(get_ext[0].toLowerCase(),exts) === -1) {
            returnValue = false;
            $('.js-upload_resume_group').addClass('error');
            $('.js-upload_resume_error').removeClass('hide');
        }
    }

    return returnValue;
}
