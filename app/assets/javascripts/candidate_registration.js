$(document).ready( function () {
    $('.js-candidate_step1_registration_submit').on('click', function (event) {
        event.preventDefault();
        if (validateForm1()) {
            $('.js-candidate_reg1').submit();
        }
    });

    $('.js-candidate_step2_registration_submit').on('click', function (event) {
        event.preventDefault();
        if (validateForm2()){
            $('.js-candidate_reg2').submit();
        }   
    });

    $('.js-candidate_step3_registration_submit').on('click', function (event) {
        event.preventDefault();
        $('.js-candidate_reg3').submit();
    });

    $('.js-candidate_locations').on('change', function (event) {
        event.preventDefault();

        if ($(this).val().length > 0) {
            fetch_and_show_cities($(this).val(), 'candidate');   
        } else {
            $('.js-cities').html('');
        }
    });

    $('.js-candidate_exp_locations').on('change', function (event) {
        event.preventDefault();

        if ($(this).val().length > 0) {
            fetch_and_show_cities($(this).val(), 'candidate_exp');   
        } else {
            $('.js-cities').html('');
        }
    });

});

function validateForm1 () {
    var returnValue = true;
    var email = $('.js-email');
    var password = $('.js-password');
    var confirmPassword = $('.js-confirm_password');
    var locations = $('.js-candidate_locations')
    var cities = $('.js-current_city')
    var file = $('input[type="file"]').val();
    var exts = ['doc','docx','rtf','pdf'];
    var phoneNumber = $('.js-phone_number')

    if (!isEmailValid(email)) {
        returnValue = false;
    }

    if (!checkLength(password,6,12)) {
        returnValue = false;
    }

    if (!checkLength(confirmPassword,6,12)) {
        returnValue = false;
    }
    
    
    if (password.val().length > 0 && confirmPassword.val().length > 0) {
        if (password.val() !== confirmPassword.val()) {
            returnValue = false;
            $('.js-confirm_password_group').addClass('error');
            $('.js-confirm_password_error').addClass('hide');
            $('.js-confirm_password_error_match').removeClass('hide');
        }    
    }
    if (locations.val().length === 0) {
        returnValue = false;

        $('.js-candidate_locations_group').addClass('error');
        $('.js-candidate_locations_error').removeClass('hide');
    }
    if (cities.length > 0 && cities.val().length === 0) {
        returnValue = false;

        $('.js-cities').addClass('error');
        $('.js-cities_error').removeClass('hide');
    }

    if (phoneNumber.val().length !== 10){
        returnValue = false;

        $('.js-phone_number_group').addClass('error');
        $('.js-phone_number_error').removeClass('hide');
    }

    if($('.js-terms_conditions').prop('checked') === false ) {
        returnValue = false;

        $('.js-terms_conditions').addClass('error');
        $('.js-checkbox_error').removeClass('hide');

    }

    if (file.length > 0) {
        var get_ext = file.split('.');
            get_ext = get_ext.reverse();
        if ($.inArray(get_ext[0].toLowerCase(),exts) === -1) {
            returnValue = false;
            $('.js-upload_resume_group').addClass('error');
            $('.js-upload_resume_error').removeClass('hide');
        }
    }
    
    return returnValue;

}


function validateForm2 () {
    var returnValue = true;
    var resumeHeadline = $('.js-resume_headline')
    var graduationYear = $('.js-graduation_year')
    var expectedLocation = $ ('.js-candidate_exp_locations')
    var expectedCity = $ ('.js-exp_city');
    var ugQualification = $('.js-ug_qualification')
    var pgQualification = $('.js-pg_qualification')
    var category = $('.js-category')
    var designation = $('.js-designation')

    if (!isEmpty(resumeHeadline)) {
        returnValue = false;
    }

    if (!isEmpty(ugQualification)) {
        returnValue = false;
    }

    if (!isEmpty(graduationYear)) {
        returnValue = false;
    }

    if (!isEmpty(pgQualification)) {
        returnValue = false;
    }

    if (!isEmpty(category)) {
        returnValue = false;
    }

    if (!isEmpty(designation)) {
        returnValue = false;
    }

    // check city present or not becuase some time it is hidden/not available
    if (expectedCity.length > 0) {
        returnValue = isEmpty(expectedCity);
    }

    return returnValue;

}
