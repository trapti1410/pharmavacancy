var all = $(".question-container");
var allcount = all.length;
$("#pall").html(allcount);

    function checkEmpty(elem){
        var opt = $(elem).val();
        var currentId = $(elem).attr("id");
        $("#"+currentId+"-required").fadeOut();
        
        if(opt==0 || opt==""){
            $("#"+currentId+"-required").fadeIn();
            return false;
        }
        else{
            $("#"+currentId+"-required").fadeOut();
            return true;
        }
    }
    function nextPage(box){
        var topBox = box.parentNode;
        var inputs = $(".question", topBox);
        var pass = true;
        for(var i=0; i<inputs.length; i++){
            if(!checkEmpty(inputs[i])){
                pass = false;
            };
        };
        if(pass){
            $(topBox).fadeOut('fast',function(){
                $(topBox).next().fadeIn();
            });
            initProgress(topBox);
        };
    }
    function initProgress(part, dir){
        var i = 0;
        var found = false;
        while(i<= allcount && !found){
            if(all[i]==part){
                var percdone = ((i+1)/allcount)*100;
                $(".progressbar").width(percdone+"%");
                $("#pstep").html(i+1);
                found = true;
            }
            i++;
        }                   
    }
    function RegisterEmail(button){
        $("#maEmailId-invalid").hide();
        var proceed = checkEmpty($("#maEmailId"));
        if(proceed){
            var email = $.trim($("#maEmailId").val());
            if (isEmail(email)){
                $.post("jobseekerreg/dupchk.html",{username : email},function(data){
                    if(parseInt(data)){
                        alert("That email address is already in use. Login or request a new password if you have forgotten it.");
                        $("#maEmailId-invalid").show();
                    }
                    else
                    {
                        $("#maEmailId-required").hide();                            
                        $("#maEmailId-invalid").hide();  
                        // var userFill = $("#maEmailId").val().split("@",1);
                        // $('#maJsName').val(userFill);
                        $('#ma_label_login_id').html($("#maEmailId").val());    
                        $('#ma_contact_number_div').slideDown('slow');  
                        nextPage(button);   
                    }
                }); 
            }
        }
    }
    //<!-- Account creation validation -->
    function RegisterCandidate(button){
        $(".required-text").hide();
        var proceed = $("#tAndC").is(":checked");
        var pass = true;
        var topBox = button.parentNode;
        var inputs = $(".question", topBox);
        
        for(var i=0; i<inputs.length; i++){
            if(!checkEmpty(inputs[i])){
                pass = false;
            };
        };
        if(pass){
            if($("#maPassword").val()!=$("#maConfirmPassword").val()){
                pass = false;
                $("#maConfirmPassword-invalid").show();
            }
        }
        if(!proceed){
            $("#tAndC-required").show();
        };
        if(pass && proceed){
            //thenn need registration..
            $("#maRegForm").submit();
        };
    }
    function isEmail(email) { 
        return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(email);
    } 
    $(document).ready(function()
    {
        $(".getStartedClass").click(function(data){
            if($(this).attr('id') == 'getStarted'){
                $(".GSTitle").show();
                $(".simpleTitle").hide();
            }else{
                $(".simpleTitle").show();
                $(".GSTitle").hide();
            }
            openPopUp("muregistration",80);
        });
        $("#maCountry").change(function(){
            var getcountry = $(this).val();
            if(getcountry == '76'){
                $("#maotherselectlist").slideUp("slow");
                $("#ma_other_city_locations").val(1);
                $("#ma_national_city_locations").html($("#maindiacityid").html()).attr("onblur","checkEmpty(this);");
                $("#manationcitylist").slideDown("slow");
            }else{
                $("#manationcitylist").slideUp("slow");
                $("#ma_other_city_locations").val('').attr("onblur","checkEmpty(this);");
                $("#ma_national_city_locations").html('').attr("onblur","");
                $("#maotherselectlist").slideDown("slow");
                //$("#national_city_locations").val("");
            }
        });
        //category for role
        $("#ma_faid").change(function(data){
            var fval = $(this).val();
            $.post("roles/getRoles.html",{fid:fval},function(data){
                $("#maRoles").html(data);
                $(".maRolesDiv").slideDown("slow");
            });
        });
    });