$(document).ready( function () {
    $('.js-change_password_submit').on('click', function (event) {
        event.preventDefault();
        if (validateChangePassword()){
        	$('.js-change_password_form').submit();
    	}
    });
});

    function validateChangePassword() {
    	var returnValue = true;

    	var oldPassword = $('.js-old_password');
    	var newPassword = $('.js-new_password');
    	var confirmNewPassword = $('.js-confirm_new_password');

    	if (!isEmpty(oldPassword)) {
            returnValue = false;
        }
    	
		if (!checkLength(newPassword,6,12)) {
            returnValue = false;
        }
		if (!checkLength(confirmNewPassword,6,12)) {
            returnValue = false;
        }

		if (newPassword.val().length > 0 && confirmNewPassword.val().length > 0) {
        	if (newPassword.val() !== confirmNewPassword.val()) {
            returnValue = false;
            $('.js-confirm_password_group').addClass('error');
            $('.js-confirm_password_error').addClass('hide');
            $('.js-confirm_password_error_match').removeClass('hide');
        }    
    }
    	return returnValue;
}