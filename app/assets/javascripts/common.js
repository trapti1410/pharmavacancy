$(document).ready( function () {
    $('.js-download_resume').on('click', function (event) {
        event.preventDefault();
        var candidateId = $(this).data('candidate');
        var url = '/download_resume';

        if (typeof candidateId !== "undefined" && candidateId > 0) {
            url += '?candidate_id=' + candidateId;
        }
        $.ajax({
            type: 'get',
            url: url,
            complete: function(response) {
                if (response.status === 200) {
                    window.open(response.responseText, '_blank');  
                } else {
                    alert("Permission denied");
                }
            }
        });
    });

});

function fetch_and_show_cities (locationId, source) {
    if (locationId !== '') {
        $.ajax({
            type: 'get',
            url: '/get_location_cities/' + locationId + '?source='+source,
            complete: function(response) {
                if (response.status === 200) {
                    $('.js-cities').html(response.responseText);    
                }
            }
        });
    }
}

function isEmpty(element) {
    var returnValue = true;
    if (element.length > 0) {
        if ($.trim(element.val()).length <= 0) {
            returnValue = false;
            element.parent().addClass('error');
            element.parent().find('.js-error').removeClass('hide');
        } else {
            element.parent().removeClass('error');
            element.parent().find('.js-error').addClass('hide');
        }
    }

    return returnValue;
}

function isEmailValid(email) {
    
    var emailValue = email.val();
    var regularExp = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var returnValue = true;

    if (emailValue.length ===0 || regularExp.test(emailValue)=== false){
        returnValue = false;
        email.parent().addClass('error');
        email.parent().find('.js-error').removeClass('hide');
    }
    else {
        email.parent().removeClass('error');
        email.parent().find('.js-error').addClass('hide');
    }

    return returnValue;
}

function checkLength(element,minlength,maxlength) {
    var returnValue = true;

    if (element.val().length < minlength || element.val().length > maxlength){
        returnValue = false;
        element.parent().addClass('error');
        element.parent().find('.js-error').removeClass('hide');
    } else {
        element.parent().removeClass('error');
        element.parent().find('.js-error').addClass('hide');

    }

    return returnValue;
}


function isGreater(element1, element2) {
    var returnValue = true;

    if ( parseInt(element1.val()) > parseInt(element2.val()) ){
        returnValue = false;
        element2.parent().addClass('error');
        element2.parent().find('.js-error').removeClass('hide');
    } else {
        element2.parent().removeClass('error');
        element2.parent().find('.js-error').addClass('hide');

    }

    return returnValue;
}

