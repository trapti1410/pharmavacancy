$(document).ready( function () {
    var tabs = $( "#menus" ).tabs();

    tabs.find( ".ui-tabs-nav" ).sortable({
        axis: "x",
        stop: function() {
          tabs.tabs( "refresh" );
        }
    });
    
    var toggle = false;
    $(".toggle").click(function(){
        var rel = $(this).attr('rel');
        if(toggle){
            $("#"+rel).slideUp(1000);
            toggle = false;
            var src1 = "/assets/images/plus-icon2.png";
            $('#toggleId').attr('src',src1);
        } else {
            var src1 = "/assets/images/minus-icon2.png";
            $('#toggleId').attr('src',src1);
            $("#"+rel).slideDown(1000);
            toggle = true;
        }
    });

    $(".toggle2").click(function(){
        var src1 = "/assets/images/minus-icon2.png";
        $('#toggleId').attr('src',src1);
        var rel = $(this).attr('rel');
        $("#"+rel).slideDown(1000);
            toggle = true;
    });
});