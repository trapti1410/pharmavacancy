$( document ).ready(function() {
    $('.js-employer_locations').on('change', function (event) {
        event.preventDefault();

        if ($(this).val().length > 0) {
            fetch_and_show_cities($(this).val());   
            
        } else {
            $('.js-cities').html('');
        }
    });

    // on click event for submit button of employer registration
    $('.js-employer_registration_submit').on('click', function (event) {
        event.preventDefault();

        var isValid = validate_employer_registration();

        // submit form only  when all data are correct
        if (isValid === true) {
            $('.js-employer_form').submit();
        }
    });

    // on click event for submit button of employer registration
    $('.js-employer_edit_submit').on('click', function (event) {
        event.preventDefault();

        var isValid = validateEmployer();

        // submit form only  when all data are correct
        if (isValid) {
            $('.js-employer_form').submit();
        }
    });

    $('.js-candidate_search').on('click', function (event) {
        event.preventDefault();

        if (validateCandidateSearch()){
            $('.js-candidate_search_form').submit();
        }
    });

    // on click event for submit button of employer registration
    $('.js-plan_submit').on('click', function (event) {
        event.preventDefault();

        // submit form only  when all data are correct
        $('.js-plan_payment_form').submit();
    });

});

function fetch_and_show_cities (locationId) {
    if (locationId !== '') {
        $.ajax({
            type: 'get',
            url: '/get_location_cities/' + locationId + '?source=employer',
            complete: function(response) {
                if (response.status === 200) {
                    $('.js-cities').html(response.responseText);    
                }
            }
        });
    }
}

// Validation for employer registration
function validate_employer_registration () {
    var returnValue = true;

    var email = $('.js-email');
    var password = $('.js-password');
    var confirm_password = $('.js-confirm_password');
    var company_name = $('.js-company_name');
    var contact_number = $('.js-contact_number');

    if (email.val().length === 0) {
        returnValue = false;
        $('.js-email_group').addClass('error');
        $('.js-email_error').removeClass('hide');
    }

    if (password.length > 0 && password.val().length === 0) {
        returnValue = false;
        $('.js-password_group').addClass('error');
        $('.js-password_error').removeClass('hide');
    }
    if (confirm_password.length > 0 && confirm_password.val().length === 0) {
        returnValue = false;
        $('.js-confirm_password_group').addClass('error');
        $('.js-confirm_password_error').removeClass('hide');
    }
    if (company_name.val().length === 0) {
        returnValue = false;
        $('.js-company_name_group').addClass('error');
        $('.js-company_name_error').removeClass('hide');
    }
    if (contact_number.val().length === 0) {
        returnValue = false;
        $('.js-contact_number_group').addClass('error');
        $('.js-contact_number_error').removeClass('hide');
    }

    return returnValue;
}

// Validation for employer registration
function validateEmployer () {
    var returnValue = true;

    var email = $('.js-email');
    var company_name = $('.js-company_name');
    var contact_number = $('.js-contact_number');

    if (email.val().length === 0) {
        returnValue = false;
        $('.js-email_group').addClass('error');
        $('.js-email_error').removeClass('hide');
    }
    if (company_name.val().length === 0) {
        returnValue = false;
        $('.js-company_name_group').addClass('error');
        $('.js-company_name_error').removeClass('hide');
    }
    if (contact_number.val().length === 0) {
        returnValue = false;
        $('.js-contact_number_group').addClass('error');
        $('.js-contact_number_error').removeClass('hide');
    }

    return returnValue;
}

function validateCandidateSearch () {
    var returnValue = true;

    var skill = $('.js-key_skills');
    var category = $('.js-category');

    if (!isEmpty(skill) && !isEmpty(category)) {
        returnValue = false;
    }

    return returnValue;

}
     