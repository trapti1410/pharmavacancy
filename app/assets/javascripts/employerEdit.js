$(document).ready( function () {
    $('js-employer_registration_submit').on('click', function (event) {
        event.preventDefault();
        if (validateEmployerEdit()){
        	$('.js-employer_form employer_form').submit();
    	}
    });
});

    function validateEmployerEdit() {
    	var returnValue = true;

    	var email = $('.js-email');
        var password = $('.js-password');
        var confirmPassword = $('.js-confirm_password');
        var companyName = $('.js-company_name');
        var contactNumber = $('.js-contact_number');

        returnValue = isEmailValid(email);
        returnValue = isEmpty(password);
        returnValue = isEmpty(confirmPassword);
        returnValue = isEmpty(companyName);
        returnValue = isEmpty(contactNumber);

        returnValue = checkLength(password,6,12);
        returnValue = checkLength(confirmPassword,6,12);

        if (password.val() !== confirmPassword.val()) {
            returnValue = false;
            $('.js-confirm_password_group').addClass('error');
            $('.js-error').addClass('hide');
            $('.js-confirm_password_error_match').removeClass('hide');
        }
    
    	
    	return returnValue;
}
