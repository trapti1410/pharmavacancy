
$(document).ready(function () {
    $('.search-drop').mouseover(function(){
        $("."+$(this).attr('data-value')).stop().css({'z-index':'100'}).slideDown('slow');
    });

    $('.main-line').mouseleave(function(){
        $("."+$(this).attr('data-value')).slideUp('slow');
    });

    $(".getStartedClass_extra").click(function(data){
        hidemodal('loginformwindow');
        openPopUp("muregistration",80);
    });

    $("#everywhereusername, #everywherepass").keydown(function(event){
        hideValidationError($(this));
    });

    $("#sumbit-login-form").click( function (event) {
        event.preventDefault();
        hideResponseError();
        if (validateLoginForm()) {
            $("#wait_div").show();
            $.ajax({
                type: 'post',
                url: '/candidate_session/perform_login',
                data : $("#everywherejslogfrm").serialize(),
                complete: function(response) {
                    $("#wait_div").hide();
                    if (response.status === 200) {
                        window.location = response.responseText;
                    } else {
                        showResponseError(response.responseText);
                    }
                }
            });

        }
    });

    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            $("#sumbit-login-form").click();
        }
    });

});

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function showResponseError(msg) {
    $('.js-response_error').html(msg).removeClass('hide');
}

function hideResponseError() {
    $('.js-response_error').addClass('hide');
}

function showValidationError (element) {
    element.parent().addClass('error');
    element.parent().find('.js-error_msg').removeClass('hide');
}

function hideValidationError (element) {
    element.parent().removeClass('error');
    element.parent().find('.js-error_msg').addClass('hide');
}

function validateLoginForm() {
    var returnValue = true;
    var userName = $('.js-user_name');
    var password = $('.js-password');

    if ($.trim(userName.val()).length === 0 || isValidEmailAddress(userName.val()) === false ) {
        returnValue = false;
        showValidationError(userName);
    } else {
        hideValidationError(userName);
    }

    if ($.trim(password.val()).length === 0) {
        returnValue = false;
        showValidationError(password);
    } else {
        hideValidationError(password);
    }

    return returnValue;
}

function closeCandidateLoginPopup () {
    hidemodal('loginformwindow');
    hideResponseError();
} 