$(document).ready( function () {
    $('.js-jobs_alert').on('click', function (event) {
        event.preventDefault();
        if (validateJobsAlert()) {
            $('.js-job_alert_form').submit();
        }

    });
});

    function validateJobsAlert() {
    	var returnValue = true;

    	var keywords = $('.js-keywords');
    	var location =$('.js-location');
    	var name = $('.js-name_job_alert');
        var email = $('.js-job_email');
        var minExpSalary = $('.js-min_exp_salary');
        var maxExpSalary = $('.js-max_exp_salary');

    	if (!isEmpty(keywords)) {
            returnValue = false;
        }

        if (!isEmpty(name)) {
            returnValue = false;
        }

        if (!isEmailValid(email)) {
            returnValue = false;
        }

        if (!isGreater(minExpSalary,maxExpSalary)) {
            returnValue = false;
        }

    	return returnValue;
    }