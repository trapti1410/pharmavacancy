$(document).ready(function (event){

	$('.admin_login_submit').on ('click', function (event) {

		event.preventDefault();

		var isValid = validate_admin_login();

		if (isValid === true){

			$('.admin_login_form').submit();
		}

	});

	$('.js-featured_employer').change(function (event) {
		event.preventDefault();
		var employerId = $(this).val();

		if (employerId != "") {
			$.ajax({
	            type: 'post',
	            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
	            url: '/admin/set_featured_employer/' + employerId,
	            complete: function(response) {
	                if (response.status === 200) {
	                    alert("Employer udpated successfully."); 
	                } else {
	                	alert("Error in updation.");
	                }
	            }
        	});
		}
	});

	$('.js-suspend_employer').change(function (event) {
		event.preventDefault();
		var employerId = $(this).val();

		if (employerId != "") {
			$.ajax({
	            type: 'post',
	            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
	            url: '/admin/suspend_user/' + employerId,
	            complete: function(response) {
	                if (response.status === 200) {
	                    alert("Employer udpated successfully."); 
	                } else {
	                	alert("Error in updation.");
	                }
	            }
        	});
		}
	});

	$('.js-suspend_candidate').change(function (event) {
		event.preventDefault();
		var employerId = $(this).val();

		if (employerId != "") {
			$.ajax({
	            type: 'post',
	            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
	            url: '/admin/suspend_user/' + employerId,
	            complete: function(response) {
	                if (response.status === 200) {
	                    alert("Candidate udpated successfully."); 
	                } else {
	                	alert("Error in updation.");
	                }
	            }
        	});
		}
	});

	$('.js-employer_plan').change(function (event) { 
		event.preventDefault();

		var planId = $(this).find('option:selected').val();

		var employerId = $(this).attr('id');

		if (employerId != "" && planId !== "") {
			$.ajax({
	            type: 'post',
	            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
	            url: '/admin/change_plan/' + employerId + '/' + planId,
	            complete: function(response) {
	                if (response.status === 200) {
	                    alert("Employer updated successfully."); 
	                } else {
	                	alert("Error in updation.");
	                }
	            }
        	});
		}

	});

	$('.js-approve_job').change(function (event) {
		event.preventDefault();
		var jobId = $(this).val();

		if (jobId != "") {
			$.ajax({
	            type: 'post',
	            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
	            url: '/admin/approve_job/' + jobId,
	            complete: function(response) {
	                if (response.status === 200) {
	                    alert("Job updated successfully."); 
	                } else {
	                	alert("Error in updation.");
	                }
	            }
        	});
		}
	});

});
	function validate_admin_login() {
		var returnValue = true;

		var userName = $('.js_user_name');
		var password = $('.js_password');

		if (userName.val().length === 0 ){
			returnValue = false;

			$('.js_user_name_group').addClass('error');
			$('.js_user_name_error').removeClass('hide');
		}
		if (password.val().length === 0) {
			returnValue = false;

			$('.js_password_group').addClass('error');
			$('.js_password_error').removeClass('hide');
		}

		return returnValue;

	}