$(document).ready(function (event){

	$('.admin_login_submit').on ('click', function (event) {

		var isValid = validate_admin_login();

		if (isValid === true){

			$('.admin_login_form').submit();
		}

	});

});
	function validate_admin_login() {
		var returnValue = true;

		var userName = $('.js_user_name');
		var password = $('.js_password');

		var trimUsername = $.trim(userName.val());
		if (trimUsername.length === 0 ){
			returnValue = false;

			$('.js_user_name_group').addClass('error');
			$('.js_user_name_error').removeClass('hide');
		}
		if (password.val().length === 0) {
			returnValue = false;

			$('.js_password_group').addClass('error');
			$('.js_password_error').removeClass('hide');
		}

		return returnValue;

	}