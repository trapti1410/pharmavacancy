$(document).ready(function(){
        // hide #back-top first
        $("#back-top").hide();
        // fade in #back-top
        $(function () {
            $(window).scroll(function () {
                //alert($(this).scrollTop());
                if ($(this).scrollTop() > 200) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-top a').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });
        $(".newsletterlink").click(function(){
            $("#newsemail").val('');
            $("#newsletterwindow").slideDown("slow");
        });
        $("#bonuspointHint").click(function(){
            $("#pointHint").slideDown("slow");
        });
        $("#addnewsletterfrm").validate({  
            rules: {                
                newsemail    :   {required  : true, email  : true}
            },         
            messages: {
                        newsemail    : 
                                {
                                    required:'<div class="errorForm">Please Specify Email address</div>',
                                    email   :'<div class="errorForm">Please Enter Valid Email Address</div>'
                                }
            },  submitHandler: function(form) {
                    $.post("home/newsletter.html",$("#addnewsletterfrm").serialize(), function(data){
                    hidemodal('newsletterwindow');
                        if(data == 1){
                            $("#mainInformationNewsMessage").html("You are now successfully subscribed with us");
                            $("#informationnewswindow").slideDown("slow").delay(1500).slideUp("slow");
                        }else{
                            //alert("0");
                            $("#mainInformationNewsMessage").html("You already subscribed with us.");
                            $("#informationnewswindow").slideDown("slow").delay(1500).slideUp("slow");
                        }
                    });
                }                                
        });
        $(".liveChat").click(function(){
            $(".jsml_label").click();
        });
    });