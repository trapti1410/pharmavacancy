$( document ).ready(function() {

    // on click event for submit button of post job by employer
    $('.js-job_post_submit').on('click', function(event) {
        event.preventDefault();
        var isValid = validateEmployerJobPost();

        if (isValid === true) {
            $('.js-post_job_form').submit();
        }

    });

    $('.js-job_locations').on('change', function (event) {
        event.preventDefault();

        if ($(this).val().length > 0) {
            fetch_and_show_job_cities($(this).val());
        } else {
            $('.js-cities').html('');
        }
    });

});

function validateEmployerJobPost() {
    var returnValue = true;
    var jobTitle = $('.js-job_title');
    var noOfVacancy = $('.js-no_of_vacancy');
    var jobDescription = $('.js-job_description');
    var minExp = $('.js-min_exp');
    var maxExp = $('.js-max_exp');
    
    if (jobTitle.val().length === 0 ){
        returnValue = false;
        $('.js-job_title_group').addClass('error');
        $('.js-job_title_error').removeClass('hide');
    }
    if (noOfVacancy.val().length === 0 ){
        returnValue = false;
        $('.js-no_of_vacancy_group').addClass('error');
        $('.js-no_of_vacancy_error').removeClass('hide');
    }
    var trimDescription = $.trim(jobDescription.val());
    if ( trimDescription.length === 0 ){
        returnValue = false;
        $('.js-job_description-group').addClass('error');
        $('.js-job_description_error').removeClass('hide');
    }

    if (minExp.val().length === 0) {
        returnValue = false;
        $('.js-exp_container').addClass('error');
        $('.js-exp_error').removeClass('hide');
    }
    if (maxExp.val().length === 0) {
        returnValue = false;
        $('.js-exp_container').addClass('error');
        $('.js-exp_error').removeClass('hide');
    }

    if ( !isGreater(minExp, maxExp ) ) {
        returnValue = false;
    }
    
    return returnValue;
}

function fetch_and_show_job_cities (locationId) {
    if (locationId !== '') {
        $.ajax({
            type: 'get',
            url: '/get_location_cities/' + locationId + '?source=job',
            complete: function(response) {
                if (response.status === 200) {
                    $('.js-cities').html(response.responseText);    
                }
            }
        });
    }
}
