$( document ).ready(function() {
    $('.js-search').on('click', function (event) {
        event.preventDefault();
        if (validateSearchForm()) {
            $('.js-search_form').submit();
        }
    });

    $('.js-refine_search').on('click', function (event) {

        event.preventDefault();

        var returnValue = true;

        var searchKeywords = $('.js-search_keywords').val();

        var searchLocation = $('.js-search_location').val();

        if ( searchKeywords.length <= 0 && searchLocation.length <= 0 ) {
            returnValue = false;
        }

        if (returnValue) {
            $('.js-search_refine_form').submit();
        }

    });

});

function validateSearchForm () {
    var returnValue = true;
    
    if ( $.trim($('.js-search_keywords').val()).length === 0 || $.trim($('.js-search_location').val().length) === 0) {
        returnValue = false;
        $('.js-search_error').removeClass('hide');
    } else {
        $('.js-search_error').addClass('hide');
    }

    return returnValue;
}