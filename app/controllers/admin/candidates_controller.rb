class Admin::CandidatesController < ApplicationController
    before_filter :verify_admin
    layout 'admin'

    def index
        @cadidates = Candidate.all
    end

    def destroy
        @candidate = Candidate.find(params[:id])
        @candidate.destroy
        respond_to do |format|
          format.html { redirect_to admin_employers_path, notice: '' }
          format.json { head :no_content }
        end
    end
end
