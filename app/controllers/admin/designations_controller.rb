class Admin::DesignationsController < ApplicationController
  before_filter :verify_admin
  before_filter :set_admin_designation, only: [:show, :edit, :update, :destroy]
  layout "admin"

  # GET /admin/designations
  # GET /admin/designations.json
  def index
    @admin_designations = Designation.all
  end

  # GET /admin/designations/1
  # GET /admin/designations/1.json
  def show
  end

  # GET /admin/designations/new
  def new
    @admin_designation = Designation.new
  end

  # GET /admin/designations/1/edit
  def edit
  end

  # POST /admin/designations
  # POST /admin/designations.json
  def create
    @admin_designation = Designation.new(admin_designation_params)

    respond_to do |format|
      if @admin_designation.save
        format.html { redirect_to admin_designations_url, notice: 'Designation was successfully created.' }
        format.json { render :show, status: :created, location: @admin_designation }
      else
        format.html { render :new }
        format.json { render json: @admin_designation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/designations/1
  # PATCH/PUT /admin/designations/1.json
  def update
    respond_to do |format|
      if @admin_designation.update_attributes(admin_designation_params)
        format.html { redirect_to admin_designations_url, notice: 'Designation was successfully updated.' }
      else
        format.html { render :edit }
        format.json { render json: @admin_designation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/designations/1
  # DELETE /admin/designations/1.json
  def destroy
    @admin_designation.destroy
    respond_to do |format|
      format.html { redirect_to admin_designations_url, notice: 'Designation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_designation
      @admin_designation = Designation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_designation_params
      params.require(:designation).permit(:name, :short_name, :vacancy_category_id)
    end
end
