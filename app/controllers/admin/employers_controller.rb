class Admin::EmployersController < ApplicationController
	before_filter :verify_admin
	layout 'admin'

	# GET /employers
	# GET /employers.json
	def index
		@employers = Employer.all
	end

	# DELETE /employers/1
	# DELETE /employers/1.json
	def destroy
		@employer = Employer.find(params[:id])
		@employer.destroy
		respond_to do |format|
		  format.html { redirect_to admin_employers_url, notice: 'Employer was successfully destroyed.' }
		  format.json { head :no_content }
		end
	end

	def set_featured_employer
		success = false
		if params[:id].present?
			employer = Employer.find params[:id]
			if employer.present?
				current_status = employer.is_featured
				employer.update_attribute(:is_featured, !current_status)
				success = true
			end
		end
		if success
			render :text => "Updated", :status => 200
		else
			render :text => "Error", :status => 500
		end
	end

	def change_plan
		if params[:id].present? && params[:plan_id].present?
			if Plan.find_by_id(params[:plan_id]).present? && (employer = Employer.find_by_id(params[:id])).present?
				employer.update_attribute(:plan_id, params[:id])
				render :text => "Updated", :status => 200
			else
				render :text => "Error", :status => 500
			end
		else 
			render :text => "Error", :status => 500
		end
	end

end
