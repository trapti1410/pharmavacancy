class Admin::IndustriesController < ApplicationController
  before_filter :verify_admin
  before_filter :set_admin_industry, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  # GET /admin/industries
  # GET /admin/industries.json
  def index
    @admin_industries = Industry.all
  end

  # GET /admin/industries/1
  # GET /admin/industries/1.json
  def show
  end

  # GET /admin/industries/new
  def new
    @admin_industry = Industry.new
  end

  # GET /admin/industries/1/edit
  def edit
  end

  # POST /admin/industries
  # POST /admin/industries.json
  def create
    @admin_industry = Industry.new(admin_industry_params)

    respond_to do |format|
      if @admin_industry.save
        format.html { redirect_to admin_industries_path, notice: 'Industry was successfully created.' }
        format.json { render :show, status: :created, location: @admin_industry }
      else
        format.html { render :new }
        format.json { render json: @admin_industry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/industries/1
  # PATCH/PUT /admin/industries/1.json
  def update
    respond_to do |format|
      if @admin_industry.update_attributes(admin_industry_params)
        format.html { redirect_to admin_industries_path, notice: 'Industry was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_industry }
      else
        format.html { render :edit }
        format.json { render json: @admin_industry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/industries/1
  # DELETE /admin/industries/1.json
  def destroy
    @admin_industry.destroy
    respond_to do |format|
      format.html { redirect_to admin_industries_url, notice: 'Industry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_industry
      @admin_industry = Industry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_industry_params
      params.require(:industry).permit(:name, :description)
    end
end
