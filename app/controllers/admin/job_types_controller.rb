class Admin::JobTypesController < ApplicationController
  before_filter :verify_admin
  before_filter :set_admin_job_type, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  # GET /admin/job_types
  # GET /admin/job_types.json
  def index
    @admin_job_types = JobType.all
  end

  # GET /admin/job_types/1
  # GET /admin/job_types/1.json
  def show
  end

  # GET /admin/job_types/new
  def new
    @admin_job_type = JobType.new
  end

  # GET /admin/job_types/1/edit
  def edit
  end

  # POST /admin/job_types
  # POST /admin/job_types.json
  def create
    @admin_job_type = JobType.new(admin_job_type_params)
    
    respond_to do |format|
      if @admin_job_type.save
        format.html { redirect_to admin_job_types_url, notice: 'Job type was successfully created.' }
        format.json { render :show, status: :created, location: @admin_job_type }
      else
        format.html { render :new }
        format.json { render json: @admin_job_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/job_types/1
  # PATCH/PUT /admin/job_types/1.json
  def update
    respond_to do |format|
      if @admin_job_type.update_attributes(admin_job_type_params)
        format.html { redirect_to admin_job_types_url, notice: 'Job type was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_job_type }
      else
        format.html { render :edit }
        format.json { render json: @admin_job_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/job_types/1
  # DELETE /admin/job_types/1.json
  def destroy
    @admin_job_type.destroy
    respond_to do |format|
      format.html { redirect_to admin_job_types_url, notice: 'Job type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_job_type
      @admin_job_type = JobType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_job_type_params
      params.require(:job_type).permit(:name, :description)
    end
end
