class Admin::JobsController < ApplicationController
    before_filter :verify_admin
    layout 'admin'

    def index
        @jobs = Job.all
    end

    def pending_jobs
        @jobs = Job.where(:approved => false)
    end

    # DELETE /jobs/1
    # DELETE /jobs/1.json
    def destroy
        @job = Job.find(params[:id])
        @job.destroy
        respond_to do |format|
          format.html { redirect_to job_jobs_path(@job), notice: 'Job was successfully destroyed.' }
          format.json { head :no_content }
        end
    end

    def approve_job
        success = false
        if params[:id].present?
            job = Job.find params[:id]
            if job.present?
                current_status = job.approved
                job.update_attribute(:approved, !current_status)
                success = true
            end
        end
        if success
            render :text => "Updated", :status => 200
        else
            render :text => "Error", :status => 500
        end
    end

end
