class Admin::LoginController < SessionController
  layout 'admin'

  def index
    redirect_to admin_dashboard_path if admin_logged_in?
  end

  def reset_password; end
end
