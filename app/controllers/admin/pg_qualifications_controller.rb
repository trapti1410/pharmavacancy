class Admin::PgQualificationsController < ApplicationController
  before_filter :verify_admin
  before_filter :set_admin_pg_qualification, only: [:show, :edit, :update, :destroy]
  layout "admin"

  # GET /admin/pg_qualifications
  # GET /admin/pg_qualifications.json
  def index
    @admin_pg_qualifications = PgQualification.all
  end

  # GET /admin/pg_qualifications/1
  # GET /admin/pg_qualifications/1.json
  def show
  end

  # GET /admin/pg_qualifications/new
  def new
    @admin_pg_qualification = PgQualification.new
  end

  # GET /admin/pg_qualifications/1/edit
  def edit
  end

  # POST /admin/pg_qualifications
  # POST /admin/pg_qualifications.json
  def create
    @admin_pg_qualification = PgQualification.new(admin_pg_qualification_params)

    respond_to do |format|
      if @admin_pg_qualification.save
        format.html { redirect_to admin_pg_qualifications_url, notice: 'Pg qualification was successfully created.' }
        format.json { render :show, status: :created, location: @admin_pg_qualification }
      else
        format.html { render :new }
        format.json { render json: @admin_pg_qualification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/pg_qualifications/1
  # PATCH/PUT /admin/pg_qualifications/1.json
  def update
    respond_to do |format|
      if @admin_pg_qualification.update_attributes(admin_pg_qualification_params)
        format.html { redirect_to admin_pg_qualifications_url, notice: 'Pg qualification was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_pg_qualification }
      else
        format.html { render :edit }
        format.json { render json: @admin_pg_qualification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/pg_qualifications/1
  # DELETE /admin/pg_qualifications/1.json
  def destroy
    @admin_pg_qualification.destroy
    respond_to do |format|
      format.html { redirect_to admin_pg_qualifications_url, notice: 'Pg qualification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_pg_qualification
      @admin_pg_qualification = PgQualification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_pg_qualification_params
      params.require(:pg_qualification).permit(:name, :description)
    end
end
