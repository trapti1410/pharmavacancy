class Admin::PlansController < ApplicationController
    before_filter :verify_admin
    before_filter :set_plan, only: [:show, :edit, :update, :destroy]
    layout "admin"

  # GET /admin/Plans
  # GET /admin/Plans.json
  def index
    @admin_plans = Plan.all
  end

  # GET /admin/Plans/1
  # GET /admin/Plans/1.json
  def show
  end

  # GET /admin/Plans/new
  def new
    @admin_plan = Plan.new
  end

  # GET /admin/Plans/1/edit
  def edit
  end

  # POST /admin/Plans
  # POST /admin/Plans.json
  def create
    @admin_plan = Plan.new(plan_params)

    respond_to do |format|
      if @admin_plan.save
        format.html { redirect_to admin_plans_url, notice: 'Plan was successfully created.' }
        format.json { render :show, status: :created, location: @admin_plan }
      else
        format.html { render :new }
        format.json { render json: @admin_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/designations/1
  # PATCH/PUT /admin/designations/1.json
  def update
    respond_to do |format|
      if @admin_plan.update_attributes(plan_params)
        format.html { redirect_to admin_plans_url, notice: 'Plan was successfully updated.' }
      else
        format.html { render :edit }
        format.json { render json: @admin_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/designations/1
  # DELETE /admin/designations/1.json
  def destroy
    @admin_plan.destroy
    respond_to do |format|
      format.html { redirect_to admin_plans_url, notice: 'plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan
      @admin_plan = Plan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plan_params
      params.require(:plan).permit(:name, :view_limit, :download_limit, :price)
    end
end
