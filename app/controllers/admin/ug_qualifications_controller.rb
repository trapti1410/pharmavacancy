class Admin::UgQualificationsController < ApplicationController
  before_filter :verify_admin
  before_filter :set_admin_ug_qualification, only: [:show, :edit, :update, :destroy]
  layout "admin"
  # GET /admin/ug_qualifications
  # GET /admin/ug_qualifications.json
  def index
    @admin_ug_qualifications = UgQualification.all
  end

  # GET /admin/ug_qualifications/1
  # GET /admin/ug_qualifications/1.json
  def show
  end

  # GET /admin/ug_qualifications/new
  def new
    @admin_ug_qualification = UgQualification.new
  end

  # GET /admin/ug_qualifications/1/edit
  def edit
  end

  # POST /admin/ug_qualifications
  # POST /admin/ug_qualifications.json
  def create
    @admin_ug_qualification = UgQualification.new(admin_ug_qualification_params)

    respond_to do |format|
      if @admin_ug_qualification.save
        format.html { redirect_to admin_ug_qualifications_url, notice: 'Ug qualification was successfully created.' }
        format.json { render :show, status: :created, location: @admin_ug_qualification }
      else
        format.html { render :new }
        format.json { render json: @admin_ug_qualification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/ug_qualifications/1
  # PATCH/PUT /admin/ug_qualifications/1.json
  def update
    respond_to do |format|
      if @admin_ug_qualification.update_attributes(admin_ug_qualification_params)
        format.html { redirect_to admin_ug_qualifications_url, notice: 'Ug qualification was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_ug_qualification }
      else
        format.html { render :edit }
        format.json { render json: @admin_ug_qualification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/ug_qualifications/1
  # DELETE /admin/ug_qualifications/1.json
  def destroy
    @admin_ug_qualification.destroy
    respond_to do |format|
      format.html { redirect_to admin_ug_qualifications_url, notice: 'Ug qualification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_ug_qualification
      @admin_ug_qualification = UgQualification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_ug_qualification_params
      params.require(:ug_qualification).permit(:name, :description)
    end
end
