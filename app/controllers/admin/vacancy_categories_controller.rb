class Admin::VacancyCategoriesController < ApplicationController
  before_filter :verify_admin
  layout 'admin'
  before_filter :set_admin_vacancy_category, only: [:show, :edit, :update, :destroy]  

  # GET /admin/vacancy_categories
  # GET /admin/vacancy_categories.json
  def index
    @admin_vacancy_categories = VacancyCategory.all
  end

  # GET /admin/vacancy_categories/new
  def new
    @admin_vacancy_category = VacancyCategory.new
  end

  # GET /admin/vacancy_categories/1/edit
  def edit
  end

  # POST /admin/vacancy_categories
  # POST /admin/vacancy_categories.json
  def create
    @admin_vacancy_category = VacancyCategory.new(admin_vacancy_category_params)

    respond_to do |format|
      if @admin_vacancy_category.save
        format.html { redirect_to admin_vacancy_categories_url, notice: 'Vacancy category was successfully created.' }
        format.json { render :show, status: :created, location: @admin_vacancy_category }
      else
        format.html { render :new }
        format.json { render json: @admin_vacancy_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/vacancy_categories/1
  # PATCH/PUT /admin/vacancy_categories/1.json
  def update
    respond_to do |format|
      if @admin_vacancy_category.update_attributes(admin_vacancy_category_params)
        format.html { redirect_to admin_vacancy_categories_url, notice: 'Vacancy category was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_vacancy_category }
      else
        format.html { render :edit }
        format.json { render json: @admin_vacancy_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/vacancy_categories/1
  # DELETE /admin/vacancy_categories/1.json
  def destroy
    @admin_vacancy_category.destroy
    respond_to do |format|
      format.html { redirect_to admin_vacancy_categories_url, notice: 'Vacancy category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_vacancy_category
      @admin_vacancy_category = VacancyCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_vacancy_category_params
      params.require(:vacancy_category).permit(:name)
    end
end
