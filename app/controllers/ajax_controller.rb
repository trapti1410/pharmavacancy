class AjaxController < ApplicationController

    def keywords
        if params[:term]
            like = "%".concat(params[:term].concat("%"))
            designations = Designation.where("name like ?", like)
            categories = VacancyCategory.where("name like ?", like)
        else
            designations = Designation.all
            categories = VacancyCategory.all
        end

        list1 = designations.map {|u| Hash[ id: u.id, label: u.name, name: u.name]}

        list12 = categories.map {|u| Hash[ id: u.id, label: u.name, name: u.name]}

        list = list1 + list12

        render json: list
    end

    def locations
        if params[:term]
            like = "%".concat(params[:term].concat("%"))
            cities = City.where("name like ?", like)
        else
            cities = City.all
        end

        list = cities.map {|u| Hash[ id: u.id, label: u.name, name: u.name]}

        render json: list
    end

    def candidates
        if params[:term]
            like = "%".concat(params[:term].concat("%"))
            candidates = Candidate.where("key_skills like ?", like)
        else
            candidates = Candidate.all
        end

        list = candidates.map {|u| Hash[ id: u.id, label: u.key_skills, name: u.key_skills]}
        list.uniq!
        render json: list
    end

end
