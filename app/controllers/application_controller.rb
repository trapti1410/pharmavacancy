class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user, :admin_logged_in?, :employer_logged_in?, :candidate_logged_in?
  helper_method :education_types, :gender, :marital_status, :language_level, :current_profile
  helper_method :months, :therapeutic_segments

  EDUCATION_TYPES = {
    1 => "Full Time",
    2 => "Part Time",
    3 => "Correspondence"
  }

  GENDER = {
    0 => "Male",
    1 => "Female"
  }

  MARITIAL_STATUS = {
    0 => "Single",
    1 => "Married",
    2 => "Widowed",
    3 => "Divorsed",
    4 => "Separated",
    5 => "Others"
  }

  LANGUAGE_LEVEL = {
    0 => "",
    1 => "Beginer",
    2 => "Proficient",
    3 => "Expert",
  }

  REGISTRATION_STATUS = {
    nil => "not_created",
    1 => "step_1",
    2 => "step_2",
    3 => "completed"
  }

  MONTHS = {                
    1 => "Jan",
    2 => "Feb",
    3 => "Apr",
    4 => "May",
    5 => "Jun",
    6 => "Jul",
    7 => "Aug",
    8 => "Sep",
    9 => "Oct",
    10 => "Nov",
    11 => "Dec"
  }

  THERAPEUTIC_SEGMENTS = {
    1 => "Cardiac",
    2 => "Diabeto",
    3 => "Ortho",
    4 => "Neuro",
    5 => "Gastro",
    6 => "Pediatric",
    7 => "Gynae",
    8 => "Dental",
    9 => "Oncology",
    10 => "ENT",
    11 => "Nephro",
    12 => "Surgeons",
    12 => "Hospital Products",
    "" => "Other"
  }

  # Redirect to logi page if admin not verified
  def verify_admin
    redirect_to admin_login_path unless admin_logged_in?
  end

  # Redirect to logi page if admin not verified
  def verify_employer
    redirect_to employer_login_path unless employer_logged_in?
    redirect_to employer_login_path if params[:employer_id].present? && params[:employer_id].to_i != session[:profile_id].to_i
  end

  def verify_candidate
    redirect_to root_path unless candidate_logged_in?

    redirect_to root_path if params[:candidate_id].present? && params[:candidate_id].to_i != session[:profile_id].to_i
  end

  # Find super admin
  def current_user
    unless session[:user_id].blank?
      @current_admin ||= User.find(session[:user_id])
    else
      return nil
    end
  end

  def admin_logged_in?
    current_user.present? && current_user.role.downcase.eql?('admin')
  end

  def employer_logged_in?
    current_user.present? && current_user.role.downcase.eql?('employer')
  end

  def candidate_logged_in?
    current_user.present? && current_user.role.downcase.eql?('candidate')
  end

  def education_types
    EDUCATION_TYPES
  end

  def gender
    GENDER
  end

  def marital_status
    MARITIAL_STATUS
  end

  def language_level
    LANGUAGE_LEVEL
  end

  def current_profile
    current_user.profile || []
  end

  def months
    MONTHS
  end

  def redirect_to_back(default = root_url)
    if !request.env["HTTP_REFERER"].blank? and request.env["HTTP_REFERER"] != request.env["REQUEST_URI"]
      redirect_to :back
    else
      redirect_to default
    end
  end

  def therapeutic_segments
    THERAPEUTIC_SEGMENTS
  end
end
