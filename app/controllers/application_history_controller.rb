class ApplicationHistoryController < ApplicationController
    layout "candidate"
    before_filter :verify_candidate
    before_filter :set_candidate

    def index
        @jobs = @candidate.jobs.paginate(:page => params[:page], :per_page => 10)
    end

    def show
        redirect_to candidate_application_history_path(@candidate) if params[:job_id].blank?
        @job = @candidate.jobs.find_by_id(params[:job_id])
        @applied_job = @candidate.candidate_draft_jobs.find_by_job_id(@job.id)
    end

    def saved_jobs
        @jobs = @candidate.saved_jobs.paginate(:page => params[:page], :per_page => 10)
    end

    private

    def set_candidate
        @candidate = Candidate.find(params[:candidate_id])
        redirect_to root_path unless @candidate.present?
    end
end
