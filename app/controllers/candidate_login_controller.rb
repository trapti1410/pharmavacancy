class CandidateLoginController < SessionController

    def perform_login
        if params[:user][:user_name].blank? || params[:user][:role].blank?
            render :text => 'Please enter valid email id.', :status => '400', :layout => false
        else
            user = User.where(:user_name => params[:user][:user_name], :password => params[:user][:password], :role => params[:user][:role]).first
            if user && user.profile.present?
                store_user(user)
                render :text => "#{candidate_path(user.profile.id)}", :status => '200', :layout => false
            else
                render :text => 'Email id and password not belongs to any account', :status => '404', :layout => false
            end
        end
    end
end