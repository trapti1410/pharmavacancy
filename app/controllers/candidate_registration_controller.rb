class CandidateRegistrationController < ApplicationController
  layout "candidate_registration"

  # GET /candidates/new
  # GET /candidates/new.json
  def new
    @candidate = Candidate.new
    @locations = Location.all
    @cities = City.all
    @ug_qualifications = UgQualification.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @candidate }
    end
  end

  def check_email_existance
    if params[:email_id].present?
      candidate = Candidate.where("email_id = ?", params[:email_id])

      if candidate.length > 0
        render :text => "Already exist", :status => '400', :layout => false
      else
        render :text => "Email id available", :status => '200', :layout => false
      end
    end
  end

  def registration_step2
    redirect_to root_path if params[:id].blank?
    @candidate = Candidate.find(params[:id])

    redirect_to root_path if @candidate.reg_status.to_i >= 2

    @ug_qualifications = UgQualification.all
    @vacancy_categories = VacancyCategory.all
    @designations = Designation.all
    @locations = Location.all
    @cities = City.all
    @pg_qualifications = PgQualification.all
  end

  def registration_step3
    redirect_to root_path if params[:id].blank?
    @candidate = Candidate.find(params[:id])
    redirect_to root_path if @candidate.reg_status.to_i >= 3
  end

  # POST /candidates
  # POST /candidates.json
  def create
    @candidate = Candidate.new(candidate_params)
    @locations = Location.all
    @cities = City.all
    @ug_qualifications = UgQualification.all

    respond_to do |format|
      if validate_form && @candidate.save
        create_candidate_user
        upload_resume
        create_job_alert(params[:job_alert]) if params[:job_alert].present?
        begin
          CandidateWelcomeMailer.welcome_email(@candidate).deliver
          CandidateWelcomeMailer.welcome_email_to_admin(@candidate).deliver
        rescue
        end
        format.html { redirect_to "/candidate_registration/#{@candidate.id}/step2", notice: 'Candidate was successfully created.' }
        format.json { render json: @candidate, status: :created, location: @candidate }
      else
        format.html { render action: "new" }
        format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /candidates/1
  # PATCH/PUT /candidates/1.json
  def step2_update
    @candidate = Candidate.find(params[:id])

    if save_candidate_profile2
      redirect_to "/candidate_registration/#{@candidate.id}/step3", notice: 'Candidate was successfully updated.'
    else
      render action: "registration_step2"
    end
  end

  # PATCH/PUT /candidates/1
  # PATCH/PUT /candidates/1.json
  def step3_update
    @candidate = Candidate.find(params[:id])

    if save_candidate_profile3
      redirect_to root_path, notice: 'Your account successfully created. Please login.'
    else
      render action: "registration_step3"
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def candidate_params
      params.require(:candidate).permit(:email_id, :full_name, :current_location_id, :phone_number, :total_exp_years, :total_exp_months, :key_skills)
    end

    def validate_form
      return_value = true
      if @candidate.email_id.blank?
        return_value = false
        @candidate.errors[:base] << "Please enter valid email id."
      else
        candidate = Candidate.where("email_id = ?", @candidate.email_id)
        if candidate.length > 0
          return_value = false
          @candidate.errors[:base] << "This email id already exist."
        end
      end

      if params[:term_conditions].blank?
        return_value = false
        @candidate.errors[:base] << "Please accept terms and conditions."
      end

      return_value = false unless validate_password

      return return_value
    end

    def validate_password
      if params[:user][:password].blank? || params[:user][:confirm_password].blank?
        @candidate.errors[:base] << "Please enter valid passwords."
        return false
      elsif params[:user][:password] != params[:user][:confirm_password]
        @candidate.errors[:base] << "Password and confirm password should be match"
        return false
      else
        return true
      end
    end

    def create_candidate_user
      user = User.new(:user_name => @candidate.email_id, :password => params[:user][:password], :role => 'candidate', :profile_id => @candidate.id, :profile_type => @candidate.class.name)
      user.verify_code = SecureRandom.uuid 
      user.save!   
    end

    def save_candidate_profile2
        # candudate save
        if params[:candidate].present?
          @candidate.update_attributes!(params[:candidate])
        end

        # education save
        if params[:education].present?
          education_params = { "education_type"=>"3", "year"=>"0"}
          
          if params[:education][:ug].present? && params[:education][:ug][:course_id].present?
            education_params.merge!({ "course_id" => params[:education][:ug][:course_id], "course_type" => "ug_qualification" })
          end

          if params[:education][:pg].present? && params[:education][:pg][:course_id].present?
            education2_params = { "course_id" => params[:education][:pg][:course_id], "course_type" => "pg_qualification" }
          end
          @candidate.educations.create(education_params)
          @candidate.educations.create(education2_params)
        end

        #employment
        if params[:employment].present?
          @candidate.employments.create(params[:employment])
        end

        if params[:candidate_profile_summary].present?
          @candidate.create_candidate_profile_summary(params[:candidate_profile_summary])
        end

        if params[:desired_profile].present?
          @candidate.create_desired_job(params[:desired_profile])
        end
    end


    def save_candidate_profile3
      # candudate save
      if params[:candidate].present?
        @candidate.update_attributes!(params[:candidate])
      end

      if params[:languages].present? && params[:languages].size > 0
        params[:languages].each do |key, language|
          if language["name"].present?
            @candidate.languages.create(language)
          end
        end
      end
    end

    def upload_resume
      return if params[:resume].blank?

      resume = @candidate.create_resume

      @attachment = DataFile.new(:resume_id => resume.id)

      @attachment.uploaded_file = params[:resume]

      @attachment.save
    end

    def load_basic_data

    end

  def create_job_alert(flag)
    if flag.to_i == 1
      keywords = @candidate.key_skills.present? ? @candidate.key_skills : "all"
      job_alert = JobAlert.create(:email_id => @candidate.email_id, :name => "#{@candidate.full_name} alert", :keywords => keywords)
    end
  end

end
