class CandidatesController < ApplicationController
  layout "candidate"
  before_filter :verify_candidate
  before_filter :set_candidate, :only => [:profile, :profile_edit, :update_profile, :destroy_profile, :change_password, :job_alert]
  before_filter :get_profile_data, :only => [:profile, :profile_edit]
  before_filter :load_basic_data, :only => [:profile_edit]

  # GET /candidates/1
  # GET /candidates/1.json
  def show
    @candidate = Candidate.find(params[:id])

    jobs = Job.all

    if @candidate.category_id.present?
      jobs = jobs.select { |j| j.functional_area_id.to_i == category_id.to_i }
    end
    @recomended_jobs = jobs.paginate(:page => params[:page], :per_page => 20)
  end

  # GET /candidates/1
  def profile; end

  def job_alert
    @job_alert = @candidate.job_alert || JobAlert.new
  end

  def profile_edit
    params[:profile_name] = params[:profile_name] || "profile_snapshot"
    render :template => "candidates/edit_forms/#{params[:profile_name]}"
  end

  def change_password; end

  # GET /candidates/1/edit
  def edit profile_name
    @candidate = Candidate.find(params[:id])
  end

  # POST /candidates
  # POST /candidates.json
  def create
    @candidate = Candidate.new(candidate_params)

    respond_to do |format|
      if @candidate.save
        format.html { redirect_to @candidate, notice: 'Candidate was successfully created.' }
        format.json { render json: @candidate, status: :created, location: @candidate }
      else
        format.html { render action: "new" }
        format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /candidates/1
  # PATCH/PUT /candidates/1.json
  def update_profile
    redirect_to candidate_profile_path(:candidate_id => @candidate.id) if params[:profile_name].blank?
    if @candidate.update_profile(params, params[:profile_name])
        redirect_to candidate_profile_path(@candidate), notice: 'Candidate was successfully updated.'
      else
        redirect_to candidate_profile_path(@candidate), notice: 'Some error to update the record.'
      end
  end

  # DELETE /admin/job_types/1
  # DELETE /admin/job_types/1.json
  def destroy_profile
    @candidate.destroy_profile(params)
    respond_to do |format|
      format.html { redirect_to candidate_profile_path(@candidate), notice: '' }
      format.json { head :no_content }
    end
  end

  private

  # Use this method to whitelist the permissible parameters. Example:
  # params.require(:person).permit(:name, :age)
  # Also, you can specialize this method with per-user checking of permissible attributes.
  def candidate_params
    params.require(:candidate).permit()
  end

  def set_candidate
    @candidate = Candidate.find(params[:candidate_id])
    redirect_to root_path unless @candidate.present?
  end

  def get_profile_data
    @candidate_profile_summary = @candidate.candidate_profile_summary
    
    @employments = @candidate.employments.sort_by { |a| a.is_present ? 0 : 1 }

    @current_employment = @candidate.employments.where(:is_present => true).first

    @current_employment = Employment.new if @current_employment.blank?

    @projects = @candidate.projects

    @skills = @candidate.skills

    @educations = @candidate.educations

    @certificates = @candidate.certificates

    @languages = @candidate.languages

    @desired_job = @candidate.desired_job

    @resume = @candidate.resume || Resume.new
  end

  def load_basic_data
    params[:profile_name] = params[:profile_name] || "profile_snapshot"
    if params[:profile_name] == "profile_snapshot"
      @designations = Designation.all
      @categories = VacancyCategory.all
      @locations = Location.all
      @cities = City.all
      @designations = Designation.all
      @ug_qualifications = UgQualification.all
      @pg_qualifications = PgQualification.all
    elsif params[:profile_name] == "profile_summary"

    elsif params[:profile_name] == "employment_details"
      @designations = Designation.all
      @categories = VacancyCategory.all
      if params[:emp_id].present?
        @employment = @candidate.employments.find(params[:emp_id])
      else
        @employment = Employment.new
      end
    elsif params[:profile_name] == "project"
      if params[:project_id].present?
        @project = @candidate.projects.find(params[:project_id])
      else
        @project = Project.new
      end
    elsif params[:profile_name] == "skill"
      if params[:skill_id].present?
        @skill = @candidate.skills.find(params[:skill_id])
      else
        @skill = Skill.new
      end
    elsif params[:profile_name] == "education"
      @ug_qualifications = UgQualification.all
      @pg_qualifications = PgQualification.all

      @ug = @candidate.ug_qualification
      @pg = @candidate.pg_qualification
    elsif params[:profile_name] == "other"
      @designations = Designation.all
    elsif params[:profile_name] == "resume"
      @resume = Resume.new if @resume.blank?
    end
  end

end
