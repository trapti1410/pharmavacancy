class CommonController < ApplicationController

    def location_cities
        if params[:id].present?
            @source = params[:source]
            @cities = Location.find(params[:id]).cities
            render :layout => false
        end
    end

    def download_resume
        returnValue = false
        if employer_logged_in?
            employer = current_user.profile
            candidate = Candidate.find(params[:candidate_id])
            if employer.present? && candidate.present? && employer.verify_download_limit
                returnValue = true
                employer.increase_profile_download(candidate)
            end
        elsif candidate_logged_in?
            returnValue = true
            candidate = current_user.profile
        end

        if returnValue
            resume = candidate.resume || Resume.new
            if resume.present? && resume.data_file.present?
                url = "/data/#{resume.data_file.file_name}"
            else
                url = ""
            end
            render :text => url, :status => '200', :layout => false
        else
            render :text => 'Permission denied.', :status => '400', :layout => false
        end
    end
end
