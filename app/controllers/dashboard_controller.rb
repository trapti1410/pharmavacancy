class DashboardController < ApplicationController
    layout :select_layout

    def index
        @vacancy_categories = VacancyCategory.all
    end

    def job_by_section
        redirect_to root_path if params[:section].blank?
        @objects = []
        case params[:section]
        when "category"
            @objects = VacancyCategory.order(:name)
        when "designation"
            @objects = Designation.order(:name)
        when "employer"
            @objects = Employer.order(:company_name)
        when "location"
            @objects = City.order(:name)
        end
        redirect_to root_path if @objects.blank?
    end

    def search
        redirect_to root_path if params[:search].blank?
        @jobs = Job.search(search_params)
        if params[:refine_search].present? && params[:refine_search][:freshness].present?
            @jobs = Job.refine_search(@jobs, params[:refine_search])
        end
    end

    def job_by_filter
        redirect_to root_path if params[:filter].blank?
        @jobs = Job.filter_by(params[:filter], params[:filter_id])
    end

    def  advanced_search
        @job_types = JobType.all
    end
    
    def perform_advanced_search
        redirect_to root_path if params[:search].blank?
        @jobs = Job.adv_search(search_params)
        render :action => :search
    end

    private
 
    def search_params
        params[:search]
    end

    def select_layout
        if action_name == "index"
            "application"
        else
            "search"
        end
    end
end
