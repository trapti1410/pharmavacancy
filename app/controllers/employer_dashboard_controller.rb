class EmployerDashboardController < ApplicationController
  before_filter :verify_employer, :except => [:login, :reset_password]
  before_filter :set_employer, :except => [:login, :reset_password]

  layout 'employer'

  def login; end

  def reset_password; end
  
  def index
    @plan = @employer.plan
  end

  def change_password; end

  def applied_candidates
    @jobs = @employer.jobs.paginate(:page => params[:page], :per_page => 10)
  end

  def candidate_profile
    @candidate = Candidate.find(params[:id])

    @employer.increase_profile_view(@candidate)

    @candidate_profile_summary = @candidate.candidate_profile_summary
    
    @current_employment = @candidate.employments.where(:is_present => true).first

    @desired_job = @candidate.desired_job

    @resume = @candidate.resume || Resume.new

    @educations = @candidate.educations
  end

  def candidates
    @candidates = Candidate.all
  end

  def perform_search
    if params[:search][:keywords].present? || params[:search][:category_id].present?
      @candidates = Candidate.search(params[:search])
      render :candidates
    else
      flash[:error] = "Please enter keywords or select category"
      redirect_to employer_candidate_search_path(@employer)
    end
  end

  def candidate_search
    @ug_qualifications = UgQualification.all
    @pg_qualifications = PgQualification.all
    @cities = City.all
    @designations = Designation.all
  end

  def upgrade_plans
    @plans = Plan.all

    @current_plan = @employer.plan
  end

  def place_order
    if params[:plan_id].present?
      @plan = Plan.find(params[:plan_id])
    else
      flash[:error] = "Unable to upgrade the plan, Please try again"
      redirect_to employer_upgrade_plans_path(@employer)
    end
  end

  def checkout
  end

  def create_order
    if params[:order][:plan_id].present? && params[:order][:plan_id].present?
      @order = Order.new(params[:order])

      @order.user_id = current_user.id

      @order.status = "created"

      @order.save!

      render :checkout
    else
      flash[:error] = "Unable to upgrade the plan, Please try again"
      redirect_to employer_upgrade_plans_path(@employer)
    end 
  end

  def confirm_order
    @notification = ActiveMerchant::Billing::Integrations::Ccavenue::Notification.new(request.raw_post)
    if @notification.payment_id.present?
      @order = Order.find_by_order_id(@notification.payment_id)
      if @notification.complete? and @notification.valid?
        @employer.update_attribute(:plan_id, @order.plan_id) if @order.confirm!
        flash[:success] = "Plan upgraded successfully"
      else
        @order.reject!
      end
    end
    redirect_to employer_upgrade_plans_path(@employer)
  end

  private

  def set_employer
    if (@employer = current_user.profile).blank?
        redirect_to employer_login_path
    end
  end

end
