class EmployersController < ApplicationController
  before_filter :verify_employer, :only => [:index, :edit]
  before_filter :set_employer, only: [:show, :edit, :update, :destroy]
  before_filter :get_related_data, only: [:new, :edit, :create, :update]
  layout 'employer'

  # GET /employers
  # GET /employers.json
  def index
    @employers = Employer.all
  end

  # GET /employers/1
  # GET /employers/1.json
  def show
  end

  # GET /employers/new
  def new
    @employer = Employer.new
  end

  # GET /employers/1/edit
  def edit; end

  # POST /employers
  # POST /employers.json
  def create
    
    @employer = Employer.new(employer_params)
    if (default_plan = Plan.find_by_is_default(true)).blank?
      render :new and return
    end
    @employer.plan_id = default_plan.id
    respond_to do |format|
      if validate_password && @employer.save
        create_employer_user
        upload_employer_logo(params[:employer_logo]) if params[:employer_logo].present?
        begin
          EmployerWelcomeMailer.welcome_email(@employer).deliver
          EmployerWelcomeMailer.welcome_email_to_admin(@employer).deliver
        rescue
        end
        format.html { redirect_to employer_login_path, notice: 'Employer was successfully created.' }
        format.json { render :show, status: :created, location: @employer }
      else
        format.html { render :new }
        format.json { render json: @employer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employers/1
  # PATCH/PUT /employers/1.json
  def update

    respond_to do |format|
      if @employer.update_attributes(employer_params)
         upload_employer_logo(params[:employer_logo]) if params[:employer_logo].present?
        format.html { redirect_to employer_dashboard_path, notice: 'Employer was successfully updated.' }
        format.json { render :show, status: :ok, location: @employer }
      else
        format.html { render :edit }
        format.json { render json: @employer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employers/1
  # DELETE /employers/1.json
  def destroy
    @employer.destroy
    respond_to do |format|
      format.html { redirect_to employer_dashboard_path, notice: 'Employer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employer
      @employer = Employer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employer_params
      params[:employer]
    end

    def create_employer_user 
      user = User.new(:user_name => @employer.email_id, :password => params[:user][:password], :role => 'employer', :profile_id => @employer.id, :profile_type => @employer.class.name)
      user.verify_code = SecureRandom.uuid 
      user.save!   
    end

    def validate_password
      return_value = true
      if @employer.email_id.blank?
        return_value = false
        @employer.errors[:base] << "Please enter valid email id."
      else
        employer = Employer.where("email_id = ?", @employer.email_id)

        if employer.length > 0
          return_value = false
          @employer.errors[:base] << "This email id already exist."
        end
      end

      if params[:user][:password].blank? || params[:user][:confirm_password].blank?
        @employer.errors[:base] << "Please enter valid passwords."
        return_value = false
      elsif params[:user][:password] != params[:user][:confirm_password]
        @employer.errors[:base] << "Password and confirm password should be match"
        return_value = false
      end
      
      return return_value
    end

    def get_related_data
      @locations = Location.all
      @cities = City.all
    end

    def upload_employer_logo incoming_file
        file_name = incoming_file.original_filename
        content_type = incoming_file.content_type
        data = incoming_file.read

        #file_name = upload['datafile'].original_filename  if  (upload['datafile'] !='')    
        #file = upload['datafile'].read

        file_name = "#{Time.now.to_i}_" + file_name

        directory = LOGO_DIRECTORY_PATH

        # create the file path
        Dir.mkdir(directory) unless File.exists?(directory)

        path = File.join(directory, file_name)
        #Dir.mkdir(image_root + "#{name_folder}")

        File.open(path, "wb")  do |f|  
            f.write(data) 
        end
        @employer.update_attribute(:logo_url, file_name)
    end
end
