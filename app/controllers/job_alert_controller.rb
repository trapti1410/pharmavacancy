class JobAlertController < ApplicationController
    layout 'search'

    def index; end

    def create
        if params[:id].present? && (candidate = Candidate.find_by_id(params[:id])).present?
            saved = false
            if (job_alert = candidate.job_alert).present?
                job_alert.update_attributes(job_alert_params)
                saved = true
            else
                job_alert = JobAlert.new(job_alert_params)
                job_alert.candidate_id = candidate.id
            end
            
            if saved || job_alert.save
                redirect_to candidate_job_alert_path(candidate), notice: 'Job Alert was successfully created.' 
            else
                render :index
            end
        else
            redirect_to root_path, notice: 'Error in user session' 
        end
    end

    private

    def job_alert_params
        params[:job_alert]
    end
end
