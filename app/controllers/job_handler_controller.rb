class JobHandlerController < ApplicationController
    layout "candidate_registration"
    before_filter :verify_candidate, :except => [:apply_to_job]
    before_filter :set_job, :only => [:apply_to_job, :apply_job]

    def apply_to_job; end

    def apply_job
        @candidate = current_user.profile
        message  = ""
        if params[:id].present? && @job.present?
            message = @candidate.job_apply(params[:id])
            if @job.employer.present?
                ApplyJobMailer.apply_email(@candidate, @job).deliver
            end
        end
        redirect_to "/job/#{params[:id]}/apply_to_job", :notice => message
    end

    def save_job
        @candidate = current_user.profile
        message  = ""
        if params[:id].present?
            message = @candidate.job_save(params[:id])
        end
        redirect_to "/job/#{params[:id]}/apply_to_job", :notice => message
    end

    private

    def set_job
        redirect_to root_path if params[:id].blank?
        @job = Job.find(params[:id])
        @employer = @job.employer
    end
end
