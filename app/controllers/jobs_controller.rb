class JobsController < ApplicationController
  before_filter :verify_employer
  before_filter :set_employer
  before_filter :set_job, only: [:show, :edit, :update, :destroy]
  before_filter :load_initial_data, :only => [:new, :create, :edit]
  layout 'employer'

  # GET /jobs
  # GET /jobs.json
  def index
    @jobs = @employer.jobs
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  # GET /jobs/new
  def new
    @job = Job.new
  end
  
  def vacancy_categories
    @VacancyCategory = VacancyCategory.new
  end

  # GET /jobs/1/edit
  def edit
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)
    @job.employer_id = @employer.id
    respond_to do |format|
      if @job.save
        format.html { redirect_to employer_jobs_path(@employer), notice: 'Job was successfully created.' }
        format.json { render :show, status: :created, location: @job }
      else
        format.html { render :new }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update_attributes(job_params)
        format.html { redirect_to employer_jobs_path(@employer), notice: 'Job was successfully updated.' }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job.destroy
    respond_to do |format|
      format.html { redirect_to employer_jobs_path(@employer), notice: 'Job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    def set_employer
      if (@employer = current_user.profile).blank?
          redirect_to employer_login_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params[:job]
    end

    def load_initial_data
      @vacancy_categories = VacancyCategory.all
      @industries = Industry.all
      @designations = Designation.all
      @ug_qualifications = UgQualification.all
      @pg_qualifications = PgQualification.all
      @locations = Location.all
      @job_types = JobType.all
      @cities = City.all
    end
end
