class SessionController < ApplicationController
  layout 'search'


  def perform_login
    if validate_login_state(params)
        store_user(@user)
        redirect_in_success(@user.role)
    else
        flash[:error] = @message
        redirect_in_error(params[:user][:role])
    end
  end

  def logout
    role = session[:role]
    remove_user
    redirect_in_success(role)
  end

  def forgot_password
  end

  def activate_user
    @message = nil
    if params[:code].present?
      user = User.where(:verify_code => params[:code]).first
      if user.present?
        user.update_attribute(:verified, true)
        @message = "Your account successfully verified, please login to access your account"
      end
    end
    @message ||= "Unable to verify your account, please contact to admin"
  end

  def send_password
    if params[:user][:email].present? && user_find
      flash[:success] = "We have sent a email to your email id with details."
    else
      flash[:error] = "Email id not valid."
    end
    redirect_to session_forgot_password_path
  end

  def user_find
    @user = User.find_by_email_id(params[:user][:email])
    @user.present?
  end

  def change_password
    if current_user.present? && valid_password_data
      if msg = current_user.update_password(user_params)
        flash[:notice] = "Your password updated successfully, please login again."
        role = session[:role]
        remove_user
        redirect_in_error(role)
      else
        flash[:error] = "Old password not correct."
        redirect_to_back
      end
    else
      flash[:error] = "Password fields are not valid."
      redirect_to_back
    end
  end

  #set session
  def store_user user
    session[:user_id] = user.id
    session[:role] = user.role
    session[:profile_id] = user.role == 'admin' ? user.id : user.profile.id
  end

  #set session
  def remove_user
    session[:user_id] = ''
    session[:role] = ''
    session[:profile_id] = ''
  end

  def redirect_in_error(role)
    case role
      when 'admin'
        redirect_to admin_login_path
      when 'employer'
        redirect_to employer_login_path
      when 'candidate'
        redirect_to root_path
    end
  end

  def redirect_in_success(role)
    case role
      when 'admin'
        redirect_to admin_dashboard_path
      when 'employer'
        redirect_to employer_dashboard_path
      when 'candidate'
        redirect_to root_path
    end
  end

  def user_params
    params[:user]
  end

  def valid_password_data
    (user_params[:old_password].present? && user_params[:password].present? && user_params[:confirm_password].present?) && (user_params[:password] == user_params[:confirm_password])
  end

  def validate_login_state params
    valid = true
    @message = ""
    if params[:user][:user_name].blank? || params[:user][:role].blank?
      valid = false
      @message = "Please enter user name."
    else
        @user = User.where(:user_name => params[:user][:user_name], :password => params[:user][:password], :role => params[:user][:role]).first
        if @user.present?
            if !@user.active
                valid = false
                @message = "Your account suspended by Admin."
            elsif !@user.verified
                valid = false
                @message = "Your account is not activated, please check your email."
            end
        else
            valid = false
            @message = "Invalid user name or password"
        end                 
    end
    valid && @message.blank?
  end

end
      