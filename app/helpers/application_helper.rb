module ApplicationHelper

    def sort_by_Jobs objects, limit
        sorted_items = objects.sort_by {|object| object.jobs.length }
        return sorted_items.reverse!.first(limit)
    end

    def generate_admin_path(object)
        if object.new_record?
            "/admin/#{object.class.name.tableize}"
        else
            "/admin/#{object.class.name.tableize}/#{object.id}"
        end
    end

    def show_date(datetime)
        datetime.present? ? datetime.strftime("%b %d, %Y") : ""
    end

    def get_categories
        @categories = VacancyCategory.limit(11)
        return @categories
    end 

    def top_categories
        sort_by_Jobs(VacancyCategory.all, 6)
    end

    def get_designations
        @designations = Designation.limit(11)
        return @designations
    end
    def top_designations
        sort_by_Jobs(Designation.all, 6)
    end

    def get_locations
        @locations = City.limit(11)
        return @locations
    end 
    def top_locations
        sort_by_Jobs(City.all, 6)
    end

    def get_hot_jobs
        Job.find(:all, :order => "created_at DESC" , :limit => 6)
    end

    def get_hot_employers
        Employer.order(:profile_views).limit(8)
    end

    def get_featured_employers
        Employer.where("is_featured = ?", true).limit(9)
    end

    def get_top_employers
        sort_by_Jobs(Employer.all, 6)
    end

    def get_ids(objects)
        objects.map(&:id)
    end

    def show_exp_range(object)
        "(#{object.experience_from}-#{object.experience_to} years)"
    end

    def get_education(id, type)
        if type == "ug_qualification"
            UgQualification.find(id).name
        else
            PgQualification.find(id).name
        end
    end

    def get_child_data object, attribute
        object.present? ? object.name : ""
    end

    def get_language(value)
        if value
            '<span class="glyphicon glyphicon-ok"></span>'
        else
            '<span class="glyphicon glyphicon-remove"></span>'
        end
    end

    def resume_download_url(resume)
        if resume.present? && resume.data_file.present?
            "/data/#{resume.data_file.file_name}"
        else
            ""
        end
    end

    def cities_name(object)
        cities = ""
        object.cities.each_with_index do |city, index|
            cities += "#{city.name}"
            cities += ", " if index+1 < object.cities.size
        end
        cities
    end

    def job_salary
        if @job.ctc_hide
            "Not disclosed by Recruiter"
        else
            ctc = ""
            ctc += @job.ctc_from.present? ? "#{number_with_precision(@job.ctc_from, :precision => 2)} - " : "0.00 - "
            ctc += @job.ctc_from.present? ? "#{number_with_precision(@job.ctc_to, :precision => 2)}" : "0.00"
            ctc += " P.A."            
        end
    end

    def job_data(object)
        if object.present?
            object.is_a?(String) ? object : object.name
        else
            "Not Available"
        end
    end

    def show_applied_date job
        if (aj = @candidate.candidate_draft_jobs.find_by_job_id(job.id)).present?
            aj.applied_at.strftime("%b %d, %Y")
        else
            ""
        end
    end

    def show_job_applied_date candidate, job
        if (aj = candidate.candidate_draft_jobs.find_by_job_id(job.id)).present?
            aj.applied_at.strftime("%b %d, %Y")
        else
            ""
        end
    end

    def show_total_experiance candidate
        exp = "#{candidate.total_exp_years} years, #{candidate.total_exp_months} months"
    end

    def section_name item
        (item.class.to_s == "Employer") ? item.company_name : item.name
    end

    def apply_to_job_path(job)
        "/job/#{job.id}/apply_to_job"
    end

    def section_url item
        if item.class.to_s == "VacancyCategory"
            "/job_by/category/#{item.id}"
        else
            "/job_by/#{item.class.to_s.downcase}/#{item.id}"
        end
    end

    def job_by_section_path section_name
        "/job_by/#{section_name}"
    end

    def truncate_description desc
        desc = desc.strip
        desc.size > 13 ? truncate(desc, :length => 12) : desc
    end

    def employer_logo_url employer
        if employer.logo_url.present?
            "logo/#{employer.logo_url}"
        else
            "logo/no-logo.png"
        end
    end

    def get_freshness
        { 
            30 => "Last 30",
            15 => "Last 15",
            7 => "Last 7",
            3 => "Last 3",
            1 => "Last 1"
        }
    end

    def expected_ctc candidate
        if candidate.desired_job.present?
            "#{candidate.desired_job.expected_sal_lakhs} lakh(s) #{candidate.desired_job.expected_sal_thousands} thousand(s)"
        end
    end

    def my_account_path
        if employer_logged_in?
            employer_dashboard_path
        elsif candidate_logged_in?
            candidate_path(:id => current_user.profile.id)
        end
    end

    def get_designation employment
        if employment.present? && employment.designation.present?
            employment.designation.name
        else
            ""
        end
    end

    def get_seagment_data object
        object.therapeutic_segment_id.present? ? therapeutic_segments[object.therapeutic_segment_id.to_i] : ''
    end

    def employer_city job
        job.employer.city.present? ? job.employer.city.name : ''
    end

    def get_download_limit e
        plan = e.plan || Plan.where(:name => "Free").first 
        plan.download_limit.to_i - e.profile_downloads.to_i
    end

    def get_view_limit e
        plan = e.plan || Plan.where(:name => "Free").first 
        plan.view_limit.to_i - e.profile_views.to_i
    end

    def resume_download_link candidate
        if (resume = candidate.resume).present? && resume.data_file.present?
            "/data/#{resume.data_file.file_name}"
        end
    end
end
