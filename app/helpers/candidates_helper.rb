module CandidatesHelper
	def selected_value(object, selectedObject)
		return "" if selectedObject.blank? || selectedObject == ""
		value = (object.present? && object.id == selectedObject.id) ? "selected" : ""
	end
end
