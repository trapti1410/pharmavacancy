class ApplyJobMailer < ActionMailer::Base
  default from: "info@pharmavacancies.com"

  def apply_email(candidate, job)
    @candidate = candidate
    @employer = job.employer
    @job = job
    email = @employer.email_id.present? ? @employer.email_id : "sibinfotech@gmail.com"
    mail(to: email, subject: 'Pharma Vacancies - applied job')
  end
end
