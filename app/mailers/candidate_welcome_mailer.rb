class CandidateWelcomeMailer < ActionMailer::Base
  default from: "info@pharmavacancies.com"

  def welcome_email(candidate)
    @candidate = candidate
    email = candidate.email_id.present? ? candidate.email_id : "sibinfotech@gmail.com"
    mail(to: email, subject: 'Welcome - Pharma Vacancies')
  end

  def welcome_email_to_admin(candidate)
    @candidate = candidate
    mail(to: "sibinfotech@gmail.com", subject: 'New Candidate Registration - Pharma Vacancies')
  end
end
