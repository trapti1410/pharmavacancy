class JobAlertMailer < ActionMailer::Base
    default from: "info@pharmavacancies.com"

  def job_alert(email_id, jobs)
    @jobs = jobs
    mail(to: email_id, subject: 'Pharma Vacancies - Job Alert')
  end

end
