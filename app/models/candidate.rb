class Candidate < ActiveRecord::Base
    email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

    has_one :user, as: :profile, dependent: :destroy
    has_one :candidate_profile_summary
    has_many :employments
    has_many :projects
    has_many :skills
    has_many :educations
    has_many :certificates
    has_many :languages
    has_one :desired_job
    has_one :resume
    has_many :employer_candidate_views
    belongs_to :city, :foreign_key => :current_city_id
    belongs_to :location, :foreign_key => :current_location_id
    belongs_to :vacancy_category, :foreign_key => :category_id
    belongs_to :designation

    validates :email_id, presence: true, format: { with: email_regex },
            :length => { :within => 5..50 }
    validates :full_name, :presence => true
    validates :current_location_id, :presence => true
    #validates :current_city_id, :presence => true
    validates :phone_number, :presence => true, length: { within: 10..11 }

    has_and_belongs_to_many :jobs, :join_table => :candidates_jobs

    has_many :candidate_draft_jobs

    has_one :job_alert

    def hightest_qualification
        education = "other"
        if (pg_education = self.educations.find_by_course_type("pg_qualification")).present?
            education = pg_education 
        elsif (ug_education = self.educations.find_by_course_type("ug_qualification")).present?
            education = ug_education
        end
        return education
    end

    def ug_qualification
        self.educations.find_by_course_type("ug_qualification")
    end

    def pg_qualification
        self.educations.find_by_course_type("pg_qualification")
    end

    def destroy_profile(params)
        if params[:profile_name] == "employment_details"
            self.employments.find(params[:profile_id]).destroy
        elsif params[:profile_name] == "project"
            self.projects.find(params[:profile_id]).destroy
        elsif params[:profile_name] == "skill"
            self.skills.find(params[:skill_id]).destroy
        elsif params[:profile_name] == "education"
            self.educations.find(params[:education_id]).destroy
        elsif params[:profile_name] == "resume"
            self.resume.destroy
        end   
    end

    def update_profile(params, profile_name)
        if profile_name == "profile_snapshot"
            update_profile_snapshot(params)
        elsif profile_name == "profile_summary"
            update_profile_summary(params)
        elsif profile_name == "employment_details"
            update_employment_details(params)
        elsif profile_name == "project"
            update_project(params)
        elsif profile_name == "skill"
            update_skill(params)
        elsif profile_name == "education"
            update_education(params)
        elsif profile_name == "other"
            update_other(params)
        elsif profile_name == "resume"
            update_resume(params)
        end          
    end

    def update_profile_snapshot(params)
        params[:candidate][:mailing_address] = params[:candidate][:mailing_address].strip if params[:candidate][:mailing_address].present?
        if self.update_attributes!(params[:candidate])
            if (current_employer_params = params[:current_employer]).present?
                cemp = self.employments.where(:is_present => true).first
                if cemp.present?
                    cemp.update_attributes!(current_employer_params)
                end
            end

            if params[:desired_profile].present?
                self.desired_job.update_attributes!(params[:desired_profile])
            end

            # education save
            if params[:education].present?
                if params[:education][:ug].present? && params[:education][:ug][:course_id].present?
                    education_params = { "course_id" => params[:education][:ug][:course_id], "course_type" => "ug_qualification" }
                    education_params[:education_type] = params[:education][:ug][:education_type]                
                    education_params[:year] = params[:education][:ug][:year]                
                end

                if params[:education][:pg].present? && params[:education][:pg][:course_id].present?
                    education2_params = { "course_id" => params[:education][:pg][:course_id], "course_type" => "pg_qualification" }
                end

                if self.ug_qualification.present?
                    self.ug_qualification.update_attributes!(education_params)
                else
                    self.educations.create(education_params)
                end

                if self.pg_qualification.present?
                    self.pg_qualification.update_attributes!(education2_params)
                else
                    self.educations.create(education2_params)
                end
            end
        end
    end

    def update_profile_summary(params)
        if params[:candidate_profile_summary].present?
            self.candidate_profile_summary.update_attributes!(params[:candidate_profile_summary])
        end
    end

    def update_employment_details(params)
        if params[:employment].present?
            if params[:emp_id].present?
                employment = self.employments.find(params[:emp_id])
                employment.update_attributes!(params[:employment])
            else
                self.employments.create(params[:employment])
            end
        end
    end

    def update_project(params)
        if params[:project].present?
            if params[:project_id].present?
                project = self.projects.find(params[:project_id])
                project.update_attributes!(params[:project])
            else
                self.projects.create(params[:project])
            end
        end
    end

    def update_skill(params)
        if params[:skill].present?
            if params[:skill_id].present?
                skill = self.skills.find(params[:skill_id])
                skill.update_attributes!(params[:skill])
            else
                self.skills.create(params[:skill])
            end
        end
    end

    def update_education(params)
        if params[:ug].present?
            if params[:ug_id].present?
                ug = self.educations.find(params[:ug_id])
                ug.update_attributes!(params[:ug])
            else
                self.educations.create(params[:ug])
            end
        end

        if params[:pg].present?
            if params[:pg_id].present?
                pg = self.educations.find(params[:pg_id])
                pg.update_attributes!(params[:pg])
            else
                self.educations.create(params[:pg])
            end
        end
    end

    def update_other(params)
        if params[:desired_job].present?
            if params[:desired_job_id].present? && params[:desired_job_id] != ""
                self.desired_job.update_attributes!(params[:desired_job])
            else
                self.create_desired_job(params[:desired_job])
            end
        end

        if params[:languages].present?
            params[:languages].each do |key, language|
                if language["id"].present?
                    clang = self.languages.find(language["id"].to_i)
                    language.delete(:id)
                    clang.update_attributes!(language)
                elsif language[:name].present?
                    language.delete(:id)
                    self.languages.create(language)
                end
            end
        end
    end

    def update_resume(params)
        if params[:resume].present?
            if params[:resume_id].present? && self.resume.present?
                
            else
                self.create_resume
            end
            upload_resume(params)
        end
    end

    def upload_resume(params)
        @attachment = DataFile.new(:resume_id => self.resume.id)

        @attachment.uploaded_file = params[:resume]

        @attachment.save
    end

    def job_apply(job_id)
        message = ""
        begin
            job = Job.find(job_id)
            if job.expiry_date.present? && job.expiry_date.to_time.to_i < Time.now.to_i
                message = "This job is expired."
            elsif self.jobs.find_by_id(job_id).present?
                message = "You have already applied for this Job."
            else
                self.jobs << job
                if (draft_job = self.candidate_draft_jobs.find_by_job_id(job_id)).present?
                    draft_job.update_attributes!(:draft => false, :applied_at => Time.now)
                else
                    self.candidate_draft_jobs.create(:job_id => job_id, :draft => false, :applied_at => Time.now)
                end
                message = "You have successfully applied for this Job."
            end
        rescue Exception => e
            #message = "Unable to apply to job."
        end
        return message
    end

    def job_save(job_id)
        message = ""
        begin
            job = Job.find(job_id)
            if job.expiry_date.present? && job.expiry_date.to_time.to_i < Time.now.to_i
                message = "This job is expired."
            elsif self.candidate_draft_jobs.find_by_id(job_id).present?
                message = "You have already saved this Job."
            elsif self.jobs.find_by_id(job_id).present?
                message = "You have already applied for this Job."
            else
                self.candidate_draft_jobs.create(:job_id => job_id)
                message = "You have successfully saved this Job."
            end
        rescue Exception => e
            message = "Unable to apply to job."
        end
        return message
    end

    def saved_jobs
        jobs = []
        self.candidate_draft_jobs.each do |dj|
            jobs << Job.find(dj.job_id) if dj.job_id.present? && dj.draft
        end
        jobs
    end

    def self.search params
        candidates = []

        if params[:keywords].present?
            candidates = Candidate.where("key_skills LIKE ? OR resume_headlines LIKE ?", "%#{params[:keywords]}%", "%#{params[:keywords]}%")
        end

        if params[:category_id].present?
            candidates += find_by_category(params[:category_id])
        end

        candidates.uniq!

        if candidates.present? && params[:designation_id].present?
            candidates = filter_by_designation(candidates, params[:designation_id])
        end

        if candidates.present? && params[:exp_from].present? && params[:exp_to].present?
            candidates = filter_by_exp(candidates, params[:exp_from], params[:exp_to])
        end

        if candidates.present? && params[:salary_lakhs_from].present? && params[:salary_lakhs_to].present?
            candidates = filter_by_salary_range(candidates, params[:salary_lakhs_from], params[:salary_lakhs_to])
        end

        if candidates.present? && params[:city_id].present?
            candidates = filter_by_city(candidates, params[:city_id])
        end

        if candidates.present? && params[:ug_id].present?
            candidates = filter_by_ug(candidates, params[:ug_id])
        end

        if candidates.present? && params[:pg_id].present?
            candidates = filter_by_pg(candidates, params[:pg_id])
        end

        candidates

    end

    def self.find_by_category category_id
        if (category = VacancyCategory.find_by_id(category_id)).present?
            category.candidates
        else
            []
        end
    end

    def self.filter_by_designation candidates, d_id
        candidates.select { |c| c.designation_id.to_i == d_id.to_i }
    end

    def self.filter_by_exp candidates, exp_from, exp_to
        candidates.select { |c| c.total_exp_years.to_i >= exp_from.to_f && c.total_exp_years.to_i <= exp_to.to_i }
    end

    def self.filter_by_salary_range candidates, min, max
        candidates.select { |c| c.salary_lakhs.to_f >= min.to_f && c.salary_lakhs.to_f <= max.to_f }
    end

    def self.filter_by_city candidates, city_id
        candidates.select { |c| c.current_city_id.to_i == city_id.to_i }
    end

    def self.filter_by_ug candidates, ug_id
        candidates.select { |c| c.educations.where(:course_type =>"ug_qualification", :course_id => ug_id).present? }
    end

    def self.filter_by_pg candidates, pg_id
        candidates.select { |c| c.educations.where(:course_type =>"pg_qualification", :course_id => pg_id ).present? }
    end

end
