class City < ActiveRecord::Base
  #attr_accessor :name, :location_id
  belongs_to :location
  validates :name, :presence => true
  validates :location_id, :presence => true

  has_and_belongs_to_many :jobs, :join_table=> :jobs_cities
end
