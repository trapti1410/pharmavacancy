class DataFile < ActiveRecord::Base
    attr_accessor :data
    belongs_to :resume

    def uploaded_file=(incoming_file)
        self.file_name = incoming_file.original_filename
        self.content_type = incoming_file.content_type
        self.data = incoming_file.read

        #file_name = upload['datafile'].original_filename  if  (upload['datafile'] !='')    
        #file = upload['datafile'].read

        self.file_name = "#{Time.now.to_i}_" + self.file_name

        directory = RESUME_DIRECTORY_PATH

        # create the file path
        Dir.mkdir(directory) unless File.exists?(directory)

        path = File.join(directory, self.file_name)
        #Dir.mkdir(image_root + "#{name_folder}")

        File.open(path, "wb")  do |f|  
            f.write(self.data) 
        end
    end

    def filename=(new_filename)
        write_attributes("file_name", sanitize_filename(new_filename))
    end

    def self.cleanup
        File.delete("#{RAILS_ROOT}/dirname/#{@filename}") if File.exist?("#{RAILS_ROOT}/dirname/#{@filename}")
    end

    private

    def santize_filename(filename)
        just_filename = File.basename(filename)
        just_filename.gsub(/[^\w\.\-]/, '_')
    end

end