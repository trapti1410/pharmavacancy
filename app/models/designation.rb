class Designation < ActiveRecord::Base
  #attr_accessor :name, :short_name
  belongs_to :vacancy_category
  validates :name, :presence => true
  validates :short_name, :presence => true
  validates :vacancy_category_id, :presence => true

  has_many :jobs, :foreign_key => :role_id
end
