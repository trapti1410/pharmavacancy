class DesiredJob < ActiveRecord::Base
	belongs_to :candidate
	belongs_to :location
	belongs_to :designation
end
