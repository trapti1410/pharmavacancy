class Employer < ActiveRecord::Base
	email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

    has_one :user, as: :profile, dependent: :destroy
    has_many :employer_candidate_views

    has_many :jobs
    belongs_to :location
    belongs_to :city
    belongs_to :plan

    validates :email_id, presence: true, format: { with: email_regex }, :length => { :within => 5..50 }
    validates :company_name, :presence => true
    validates :contact_number, :presence => true

    def increase_profile_view(candidate)
        if (vc = views_record(candidate)).blank?
        	counter = self.profile_views.to_i + 1
            self.update_attribute(:profile_views, counter)
            self.employer_candidate_views.create(:candidate_id => candidate.id, :views => 1)
        else
            if vc.views == 0
                counter = self.profile_views.to_i + 1
                self.update_attribute(:profile_views, counter)
            end
            cr = vc.views.to_i + 1
            vc.update_attribute(:views, cr)
        end

    end

    def increase_profile_download(candidate)
        if (vc = views_record(candidate)).blank?
            counter = self.profile_downloads.to_i + 1
            self.update_attribute(:profile_downloads, counter)
            self.employer_candidate_views.create(:candidate_id => candidate.id, :downloads => 1)
        else
            if vc.downloads == 0
                counter = self.profile_downloads.to_i + 1
                self.update_attribute(:profile_downloads, counter)
            end
            cr = vc.downloads.to_i + 1
            vc.update_attribute(:downloads, cr)
        end
    end

    def candidate_view(candidate)
        self.employer_candidate_views.where(:candidate_id => candidate.id, :views => 0).first
    end

    def views_record(candidate)
        self.employer_candidate_views.where(:candidate_id => candidate.id).first
    end

    def verify_view_limit
        self.profile_views.to_i < self.plan.view_limit.to_i
    end

    def verify_download_limit
        self.profile_downloads.to_i < self.plan.download_limit.to_i
    end
end
