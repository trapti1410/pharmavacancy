class EmployerCandidateView < ActiveRecord::Base
	belongs_to :candidate
	belongs_to :employer

	validates :candidate_id, :presence => true
	validates :employer_id, :presence => true
end
