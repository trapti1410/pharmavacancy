class Employment < ActiveRecord::Base
	belongs_to :candidate
	belongs_to :designation

	belongs_to :vacancy_category, :foreign_key => :category_id

	def total_duration
		if end_year != 0
			total_months = (end_year.to_i * 12 + end_month.to_i) - (start_year.to_i * 12 + start_month.to_i)
		else
			total_months = (Time.now.year.to_i * 12 + Time.now.month.to_i) - (start_year.to_i * 12 + start_month.to_i)
		end
		"#{total_months/12} year(s), #{total_months%12} month(s)"
	end
end
