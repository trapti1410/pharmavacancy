class Job < ActiveRecord::Base
  #attr_accessor :job_title, :job_type, :no_of_vacancies, :job_description, :work_experience, :vacancy_categories, :designation, :industry, :keywords, :location, :city, :ctc, :job_expiry_date, :candidate_profile, :ug_qualification, :pg_qualification
  
  validates :title, :presence => true
  validates :job_type_id, :presence => true
  validates :no_of_vacancies, :presence => true
  validates :description, :presence => true
  validates :experience_from, :presence => true    
  validates :experience_to, :presence => true
  validate  :validate_experience_duration
  validate  :validate_ctc_range

  has_and_belongs_to_many :cities, :join_table=> :jobs_cities
  has_and_belongs_to_many :candidates, :join_table => :candidates_jobs
  belongs_to :employer
  belongs_to :job_type
  belongs_to :designation, :foreign_key => :role_id
  belongs_to :vacancy_category, :foreign_key => :functional_area_id
  belongs_to :location
  belongs_to :industry
  belongs_to :ug_qualification
  belongs_to :pg_qualification
  

  def validate_experience_duration
    if experience_from.present? && experience_to.present?
      if experience_from.to_f > experience_to.to_f
        errors.add(:experience_from, "Experience from should not greater than to")
      end
    end
  end

  def validate_ctc_range
    if ctc_from.present? && ctc_to.present?
      if ctc_from.to_f > ctc_to.to_f
        errors.add(:ctc_from, "CTC from should not greater than to")
      end
    end
  end

  def self.search params
    jobs = []

    if params[:keywords].present?
      jobs = Job.where("keywords LIKE ?", "%#{params[:keywords]}%")

      jobs += find_by_employer(params[:keywords])

      jobs += find_by_designation(params[:keywords])

    end
    jobs.uniq!

    if jobs.present? && params[:location].present?
      jobs = filter_by_cities(jobs, params[:location])
    end

    if jobs.present? && params[:experience].present?
      jobs = filter_by_experience(jobs, params[:experience])
    end

    if jobs.present? && params[:salary].present?
      jobs = filter_by_salary(jobs, params[:salary])
    end

    jobs

  end

  def self.adv_search params
      jobs = []

      if params[:keywords].present?
        jobs = Job.where("keywords LIKE ?", "%#{params[:keywords]}%")

        jobs += find_by_employer(params[:keywords])

        jobs += find_by_designation(params[:keywords])

      end
      jobs.uniq!

      if jobs.present? && params[:category_id].present?
        jobs = filter_by_category(jobs, params[:category_id])
      end

      if jobs.present? && params[:location].present?
        jobs = filter_by_cities(jobs, params[:location])
      end

      if jobs.present? && params[:exp_from].present?
        experience = params[:exp_from] + 1
        jobs = filter_by_experience(jobs, experience)
      end

      if jobs.present? && params[:min_salary].present? && params[:max_salary].present?
        jobs = filter_by_salary_range(jobs, params[:min_salary], params[:max_salary])
      end
      jobs

  end

  def self.find_by_employer keyword
    jobs = []
    employers = Employer.where("company_name LIKE ?", "%#{keyword}%")
    employers.each do |e|
      jobs += e.jobs
    end
    jobs
  end

  def self.find_by_designation keyword
    jobs = []
    designations = Designation.where("name LIKE ?", "%#{keyword}%")

    designations.each do |d|
      jobs += d.jobs 
    end
    jobs
  end

  def self.find_by_cities name
    jobs = []
    cities = City.where("name LIKE ?", "%b%")

    cities.each do |c|
      jobs += c.jobs 
    end
    jobs
  end

  def self.filter_by_cities jobs, name
    city_jobs = []
    cities = City.where("name LIKE ?", "%b%")

    cities.each do |c|
      city_jobs += c.jobs
    end
    jobs & city_jobs
  end

  def self.filter_by_experience jobs, experience
    jobs.select { |job| job.experience_from.to_i <= experience.to_i }
  end

  def self.filter_by_salary jobs, salary
    jobs.select { |job| job.ctc_from.to_f <= salary.to_f }
  end

  def self.filter_by_salary_range jobs, min, max
    jobs.select { |job| job.ctc_from.to_f >= min.to_f && job.ctc_to.to_f <= max.to_f }
  end

  def self.filter_by filter_name, filter_id
    jobs = []
    case filter_name
    when "category"
      if (c = VacancyCategory.find_by_id(filter_id.to_i)).present?
        jobs = c.jobs
      end
    when "designation"
      if (d = Designation.find_by_id(filter_id.to_i)).present?
        jobs = d.jobs
      end
    when "employer"
      if (e = Employer.find_by_id(filter_id.to_i)).present?
        jobs = e.jobs
      end
    when "location"
      if (l = City.find_by_id(filter_id.to_i)).present?
        jobs = l.jobs
      end
    when "city"
      if (l = City.find_by_id(filter_id.to_i)).present?
        jobs = l.jobs
      end
    end

    jobs
  end

  def self.refine_search jobs, refine_params
    no_days = refine_params[:freshness].to_i 
    jobs.select { |j| j.created_at > no_days.days.ago}
  end

  def self.filter_by_category jobs, category_id
    jobs.select { |j| j.functional_area_id.to_i == category_id.to_i }
  end

  def self.job_alert_search job_alert
      jobs = []

      if job_alert.keywords.present?
        jobs = Job.where("keywords LIKE ?", "%#{job_alert.keywords}%")
      end

      jobs.uniq!

      if jobs.present? && job_alert.location.present?
        jobs = filter_by_cities(jobs, job_alert.location)
      end

      if jobs.present? && job_alert.work_experience_years.present?
        jobs = filter_by_experience(jobs, job_alert.work_experience_years)
      end

      if jobs.present? && job_alert.ectc_from.present? && job_alert.ectc_to.present?
        jobs = filter_by_salary_range(jobs, job_alert.ectc_from, job_alert.ectc_to)
      end

      jobs

  end

end
