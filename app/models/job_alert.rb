class JobAlert < ActiveRecord::Base
    validates :email_id, :presence => true
    validates :name, :presence => true
    validates :keywords, :presence => true

    belongs_to :candidate
end
