class JobType < ActiveRecord::Base
  
  #attr_accessor :name, :description

  validates :name, :presence => true

  has_many :jobs
end
