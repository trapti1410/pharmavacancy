class Location < ActiveRecord::Base
	
  #attr_accessor :name, :short_name

  validates :name, :presence => true
  has_many :cities
end
