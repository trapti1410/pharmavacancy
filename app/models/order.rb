class Order < ActiveRecord::Base
	belongs_to :user

	belongs_to :plan

	def confirm!
		self.update_attribute(:status, "completed")	
	end

	def reject!
		self.update_attribute(:status, "rejected")
	end
end
