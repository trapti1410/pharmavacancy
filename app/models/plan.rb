class Plan < ActiveRecord::Base
	validates :name, :presence => true
	validates :view_limit,  numericality: {greater_than_or_equal_to: 1}, presence: true
	validates :download_limit,  numericality: {greater_than_or_equal_to: 1}, presence: true

	has_many :employers
end
