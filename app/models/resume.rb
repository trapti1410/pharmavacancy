class Resume < ActiveRecord::Base
	belongs_to :candidate
	has_one :data_file, :dependent => :destroy
end
