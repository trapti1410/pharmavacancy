class User < ActiveRecord::Base
	belongs_to :profile, polymorphic: true
	attr_accessible :user_name, :password, :role, :profile_id, :profile_type
	attr_accessor :confirm_password

	validates :user_name, :presence => true, :format => { :with => /^[^@][\w.-]+@[\w.-]+[.][a-z]{2,4}$/i },
            :uniqueness => { :case_sensitive => false }, :length => { :within => 5..50 }
	validates :password, :presence => true

	def update_password params
		if self.password == params[:old_password]
			self.update_attribute(:password, params[:password])
		else
			return false
		end
	end
end
