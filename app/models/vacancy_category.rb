class VacancyCategory < ActiveRecord::Base
  #attr_accessor :name
  validates :name, :presence => true
  has_many :designations

  has_many :jobs, :foreign_key => :functional_area_id

  has_many :candidates, :foreign_key => :category_id
end
