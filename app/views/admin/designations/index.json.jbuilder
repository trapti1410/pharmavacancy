json.array!(@admin_designations) do |admin_designation|
  json.extract! admin_designation, :id, :name, :short_name, :vacancy_category_id
  json.url admin_designation_url(admin_designation, format: :json)
end
