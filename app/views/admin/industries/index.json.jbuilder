json.array!(@admin_industries) do |admin_industry|
  json.extract! admin_industry, :id, :name, :description
  json.url admin_industry_url(admin_industry, format: :json)
end
