json.array!(@admin_job_types) do |admin_job_type|
  json.extract! admin_job_type, :id, :name, :description
  json.url admin_job_type_url(admin_job_type, format: :json)
end
