json.array!(@admin_pg_qualifications) do |admin_pg_qualification|
  json.extract! admin_pg_qualification, :id, :name, :description
  json.url admin_pg_qualification_url(admin_pg_qualification, format: :json)
end
