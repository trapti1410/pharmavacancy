json.array!(@admin_ug_qualifications) do |admin_ug_qualification|
  json.extract! admin_ug_qualification, :id, :name, :description
  json.url admin_ug_qualification_url(admin_ug_qualification, format: :json)
end
