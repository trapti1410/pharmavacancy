json.array!(@admin_vacancy_categories) do |admin_vacancy_category|
  json.extract! admin_vacancy_category, :id, :name
  json.url admin_vacancy_category_url(admin_vacancy_category, format: :json)
end
