Pharmavacancy::Application.routes.draw do

  get "job_alert" => "job_alert#index"
  post "job_alert" => "job_alert#create"
  # autocomplete
  get "ajax/keywords"
  get "ajax/locations"
  get "ajax/candidates"

  get "job/:id/apply_to_job" => "job_handler#apply_to_job"
  get "job/:id/apply_job" => "job_handler#apply_job"
  get "job/:id/save_job" => "job_handler#save_job"
  get "download_resume" => "common#download_resume"

  resources :candidates, :only => [:show, :edit, :update] do 
    match '/profile' => 'candidates#profile'
    match '/profile/edit/:profile_name' => 'candidates#profile_edit'
    post '/candidate_update/:profile_name' => 'candidates#update_profile'
    delete '/candidate_destroy/:profile_name/:profile_id' => 'candidates#destroy_profile'
    get "/application_history" => "application_history#index"
    get "/application_history/show/:job_id" => "application_history#show"
    get "/saved_jobs" => "application_history#saved_jobs"
    get "/change_password" => "candidates#change_password"
    get "/job_alert" => "candidates#job_alert"
  end

  get 'candidate_registration/new' => 'candidate_registration#new'
  get 'candidate_registration/:id/step2' => 'candidate_registration#registration_step2'
  get 'candidate_registration/:id/step3' => 'candidate_registration#registration_step3'
  post 'candidate_session/perform_login' => 'candidate_login#perform_login'
  post 'candidate_registration/step1' => 'candidate_registration#create'
  put 'candidate_registration/:id/step2' => 'candidate_registration#step2_update'
  put 'candidate_registration/:id/step3' => 'candidate_registration#step3_update'

  get 'employer/login' => 'employer_dashboard#login'
  get 'employer/reset_password' => 'employer_dashboard#reset_password'
  post 'perform_login' => 'session#perform_login'
  get 'employer_dashboard' => 'employer_dashboard#index'
  get 'employer/:id/change_password' => 'employer_dashboard#change_password'
  get 'employer/:id/jobs' => 'employer_dashboard#change_password'
  get 'session/logout' => 'session#logout'
  post 'session/change_password' => 'session#change_password'
  get 'dashboard' => 'dashboard#index'
  get 'get_location_cities/:id' => 'common#location_cities'
  get 'session/forgot_password' => 'session#forgot_password'
  post 'session/send_password' => 'session#send_password'
  get 'activate/:code' => 'session#activate_user'

  resources :employers do 
    get "/applied_candidates" => "employer_dashboard#applied_candidates"
    get "/candidate_profile/:id" => "employer_dashboard#candidate_profile"
    get "/candidates" => "employer_dashboard#candidates"
    get "/candidate_search" => "employer_dashboard#candidate_search"
    post "/perform_search" => "employer_dashboard#perform_search"
    get "/upgrade_plans" => "employer_dashboard#upgrade_plans"
    get "/place_order/:plan_id" => "employer_dashboard#place_order"
    post "/create_order" => "employer_dashboard#create_order"
    get "/confirm_order/:order_id" => "employer_dashboard#confirm_order"
    resources :jobs
  end

  namespace :admin do
    resources :users
    resources :industries
    resources :employers, :only => [:index, :destroy]
    resources :jobs, :only => [:index, :destroy]
    get 'pending_jobs' => "jobs#pending_jobs"
    post "approve_job/:id" => "jobs#approve_job"
    get '/' => 'login#index'
    get 'login' => 'login#index'
    post 'perform_login' => 'login#perform_login'
    get 'logout' => 'login#logout'
    get 'dashboard' => 'dashboard#index'
    get 'reset_password' => 'login#reset_password'
    resources :cities
    resources :locations
    resources :job_types
    resources :pg_qualifications
    resources :ug_qualifications
    resources :designations
    resources :vacancy_categories
    resources :plans
    resources :candidates, :only => [:index, :destroy]
    post "set_featured_employer/:id" => "employers#set_featured_employer"
    post "suspend_user/:id" => "users#suspend_user"
    post "change_plan/:id/:plan_id" => "employers#change_plan"
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  get '/job_by/:section' => "dashboard#job_by_section"
  get '/job_by/:filter/:filter_id' => "dashboard#job_by_filter"
  post 'search' => "dashboard#search"
  get '/advanced_search' => "dashboard#advanced_search"
  post '/advanced_search' => "dashboard#perform_advanced_search"

  # You can have the root of your site routed with "root"
  root :to => 'dashboard#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
