class CreateAdminUsers < ActiveRecord::Migration
  def change
    create_table :admin_users do |t|
      t.string :user_name
      t.string :password

      t.timestamps null: false
    end
  end
end
