class CreateEmployers < ActiveRecord::Migration
  def change
    create_table :employers do |t|
      t.string :email_id
      t.string :company_name
      t.string :contact_number
      t.string :company_type
      t.integer :industry_id
      t.string :contact_person
      t.text :about_company
      t.string :website
      t.string :logo_url
      t.timestamps null: false
    end
  end
end
