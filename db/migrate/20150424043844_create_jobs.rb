class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.integer :job_type_id
      t.integer :no_of_vacancies
      t.text :description
      t.integer :experience_from
      t.integer :experience_to
      t.integer :functional_area_id
      t.integer :role_id
      t.integer :industry_id
      t.integer :location_id
      t.decimal :ctc_from, :precision => 4, :scale => 2
      t.decimal :ctc_to, :precision => 4, :scale => 2
      t.boolean :ctc_hide, :default => false
      t.date :expiry_date
      t.text :desired_candidate_profile
      t.integer :ug_qualification_id
      t.integer :pg_qualification_id      
      t.timestamps null: false
    end
  end
end
