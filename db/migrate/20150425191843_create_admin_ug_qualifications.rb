class CreateAdminUgQualifications < ActiveRecord::Migration
  def change
    create_table :admin_ug_qualifications do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
