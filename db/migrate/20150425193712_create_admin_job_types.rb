class CreateAdminJobTypes < ActiveRecord::Migration
  def change
    create_table :admin_job_types do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
