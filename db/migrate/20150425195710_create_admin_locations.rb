class CreateAdminLocations < ActiveRecord::Migration
  def change
    create_table :admin_locations do |t|
      t.string :name
      t.string :short_name

      t.timestamps null: false
    end
  end
end
