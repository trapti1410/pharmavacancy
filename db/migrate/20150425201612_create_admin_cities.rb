class CreateAdminCities < ActiveRecord::Migration
  def change
    create_table :admin_cities do |t|
      t.string :name
      t.integer :location_id

      t.timestamps null: false
    end
  end
end
