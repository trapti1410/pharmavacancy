class AddFiledsEmployer < ActiveRecord::Migration
  def change
  	add_column :employers, :address, :text
  	add_column :employers, :location_id, :integer
  	add_column :employers, :pincode, :string
  end
end
