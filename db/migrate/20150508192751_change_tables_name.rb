class ChangeTablesName < ActiveRecord::Migration
  def change
  	#drop_table :vacancy_categories
  	drop_table :industries
  	rename_table :admin_vacancy_categories, :vacancy_categories
  	rename_table :admin_designations, :designations
  	rename_table :admin_ug_qualifications, :ug_qualifications
  	rename_table :admin_pg_qualifications, :pg_qualifications
  	rename_table :admin_job_types, :job_types
  	rename_table :admin_locations, :locations
  	rename_table :admin_cities, :cities
  	rename_table :admin_industries, :industries
  end
end
