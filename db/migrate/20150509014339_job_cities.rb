class JobCities < ActiveRecord::Migration
  def change
  	create_table :jobs_cities, :id => false do |t|
      t.integer :job_id
      t.integer :city_id
    end

    add_index :jobs_cities, [:job_id, :city_id]
  end
end
