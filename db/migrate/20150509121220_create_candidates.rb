class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
        t.string :email_id
        t.string :full_name
        t.string :marital_status
        t.string :gender
        t.integer :dob_day
        t.integer :dob_month
        t.integer :dob_year
        t.string :phone_number
        t.text :mailing_address

        t.integer :current_location_id
        t.integer :current_city_id

        t.integer :total_exp_years
        t.integer :total_exp_months

        t.text :resume_headlines
        t.integer :salary_lakhs, :default => 0
        t.integer :salary_thousands, :default => 0
        t.string :key_skills

        t.string :landline_number
        t.text :permanent_address
        t.string :home_town
        t.string :home_town_pincode
        
        t.integer :category_id
        t.integer :designation_id

        t.timestamps
    end
  end
end
