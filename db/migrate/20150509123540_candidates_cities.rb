class CandidatesCities < ActiveRecord::Migration
  def up
	create_table :candidates_cities, :id => false do |t|
	t.integer :candidate_id
	t.integer :city_id
	end

	add_index :candidates_cities, [:candidate_id, :city_id]
  end

  def down
  	drop_table :candidates_cities
  end
end
