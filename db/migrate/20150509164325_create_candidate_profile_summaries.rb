class CreateCandidateProfileSummaries < ActiveRecord::Migration
  def change
    create_table :candidate_profile_summaries do |t|
    	t.text :summary
    	t.integer :candidate_id
     	t.timestamps
    end
  end
end
