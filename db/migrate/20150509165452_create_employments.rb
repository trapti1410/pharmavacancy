class CreateEmployments < ActiveRecord::Migration
  def change
    create_table :employments do |t|
        t.integer :candidate_id
        t.string :employer_name
        t.integer :employer_type_id
        t.integer :start_month
        t.integer :start_year
        t.integer :end_month
        t.integer :end_year
        t.text :job_profile
        t.integer :designation_id

        t.timestamps
    end
  end
end
