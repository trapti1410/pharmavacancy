class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
    	t.integer :candidate_id
    	t.string :client_name
    	t.string :title
    	t.integer :start_month
    	t.integer :start_year
    	t.integer :end_month
    	t.integer :end_year
    	t.text :details
    	t.text :role_description
    	t.integer :team_size
    	t.string :skills

      t.timestamps
    end
  end
end
