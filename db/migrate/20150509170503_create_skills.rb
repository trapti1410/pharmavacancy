class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
    	t.integer :candidate_id
    	t.string :name
    	t.integer :last_used
    	t.integer :exp_months
    	t.integer :exp_years

      t.timestamps
    end
  end
end
