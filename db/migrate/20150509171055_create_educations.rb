class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
    	t.integer :candidate_id
    	t.string :course_type
    	t.integer :course_id
    	t.integer :education_type
    	t.string :specialization
    	t.integer :year

      t.timestamps
    end
  end
end
