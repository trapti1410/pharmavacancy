class CreateCertificates < ActiveRecord::Migration
  def change
    create_table :certificates do |t|
    	t.integer :candidate_id
    	t.string :name

      t.timestamps
    end
  end
end
