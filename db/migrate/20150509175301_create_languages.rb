class CreateLanguages < ActiveRecord::Migration
  def change
    create_table :languages do |t|
    	t.integer :candidate_id
    	t.string :name
    	t.integer :proficiency_level
    	t.boolean :read
    	t.boolean :write
    	t.boolean :speak

      t.timestamps
    end
  end
end
