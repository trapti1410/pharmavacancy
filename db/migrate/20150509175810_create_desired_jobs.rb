class CreateDesiredJobs < ActiveRecord::Migration
  def change
    create_table :desired_jobs do |t|
    	t.integer :candidate_id
    	t.boolean :full_time
    	t.boolean :part_time
    	t.boolean :permament
    	t.boolean :contractual
    	t.integer :designation_id

      t.timestamps
    end
  end
end
