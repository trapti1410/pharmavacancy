class CreateResumes < ActiveRecord::Migration
  def change
    create_table :resumes do |t|
    	t.integer :candidate_id
    	t.text :html_format
    	t.string :url

      t.timestamps
    end
  end
end
