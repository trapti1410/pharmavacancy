class AddEmployerInJob < ActiveRecord::Migration
  def up
  	add_column :jobs, :employer_id, :integer
  end

  def down
  	remove_column :jobs, :employer_id
  end
end
