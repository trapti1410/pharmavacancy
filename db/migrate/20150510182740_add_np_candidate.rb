class AddNpCandidate < ActiveRecord::Migration
  def up
  	add_column :candidates, :notice_period, :string
  end

  def down
  	remove_column :candidates, :notice_period
  end
end
