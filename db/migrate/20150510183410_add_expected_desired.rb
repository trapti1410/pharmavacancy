class AddExpectedDesired < ActiveRecord::Migration
  def up
  	add_column :desired_jobs, :expected_sal_lakhs, :integer
  	add_column :desired_jobs, :expected_sal_thousands, :integer
  	add_column :desired_jobs, :city_id, :integer
  	add_column :desired_jobs, :location_id, :integer
  	add_column :desired_jobs, :change_reason, :text
  	add_column :desired_jobs, :therapeutic_segment, :text
  end

  def down
  	remove_column :desired_jobs, :expected_sal_lakhs
  	remove_column :desired_jobs, :expected_sal_thousands
  	remove_column :desired_jobs, :city_id
  	remove_column :desired_jobs, :location_id
  	remove_column :desired_jobs, :change_reason
  	remove_column :desired_jobs, :therapeutic_segment
  end
end
