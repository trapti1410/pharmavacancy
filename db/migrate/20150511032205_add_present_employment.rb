class AddPresentEmployment < ActiveRecord::Migration
  def up
  	add_column :employments, :is_present, :boolean
  end

  def down
  	remove_column :employments, :is_present
  end
end
