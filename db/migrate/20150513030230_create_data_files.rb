class CreateDataFiles < ActiveRecord::Migration
  def change
    create_table :data_files do |t|
      t.integer :candidate_id
      t.string :file_name
      t.string :content_type

      t.timestamps
    end
  end
end
