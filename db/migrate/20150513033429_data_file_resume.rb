class DataFileResume < ActiveRecord::Migration
  def up
  	remove_column :data_files, :candidate_id
  	add_column :data_files, :resume_id, :integer
  end

  def down
  	add_column :data_files, :candidate_id, :integer
  	remove_column :data_files, :resume_id
  end
end
