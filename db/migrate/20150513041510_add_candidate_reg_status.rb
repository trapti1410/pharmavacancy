class AddCandidateRegStatus < ActiveRecord::Migration
  def up
  	add_column :candidates, :reg_status, :integer
  end

  def down
  	remove_column :candidates, :reg_status
  end
end
