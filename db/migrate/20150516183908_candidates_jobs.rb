class CandidatesJobs < ActiveRecord::Migration
  def up
  	create_table :candidates_jobs, :id => false do |t|
  		t.integer :candidate_id
  		t.integer :job_id
  	end

	   add_index :candidates_jobs, [:candidate_id, :job_id]
  end

  def down
  	drop_table :candidates_jobs
  end
end
