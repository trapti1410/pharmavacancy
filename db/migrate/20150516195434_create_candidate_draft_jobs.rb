class CreateCandidateDraftJobs < ActiveRecord::Migration
  def change
    create_table :candidate_draft_jobs do |t|
      t.integer :job_id
      t.integer :candidate_id
      t.boolean :draft, :default => true

      t.timestamps
    end
  end
end
