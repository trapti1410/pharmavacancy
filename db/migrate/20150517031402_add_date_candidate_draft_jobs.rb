class AddDateCandidateDraftJobs < ActiveRecord::Migration
  def up
  	add_column :candidate_draft_jobs, :applied_at, :datetime
  end

  def down
  	remove_column :candidate_draft_jobs, :applied_at
  end
end
