class AddFieldsEmployer < ActiveRecord::Migration
  def up
  	add_column :employers, :plan_id, :integer, :default => 1
  	add_column :employers, :profile_views, :integer, :default => 0
  	add_column :employers, :profile_downloads, :integer, :default => 0
  end

  def down
  	remove_column :employers, :plan_id
  	remove_column :employers, :profile_views
  	remove_column :employers, :profile_downloads
  end
end
