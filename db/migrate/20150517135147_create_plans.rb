class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name
      t.integer :view_limit, :default => 0
      t.integer :download_limit, :default => 0

      t.timestamps
    end
  end
end
