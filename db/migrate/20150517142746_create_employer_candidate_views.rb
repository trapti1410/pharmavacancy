class CreateEmployerCandidateViews < ActiveRecord::Migration
  def change
    create_table :employer_candidate_views do |t|
      t.integer :candidate_id
      t.integer :employer_id
      t.integer :downloads, :default => 0
      t.integer :views, :default => 0

      t.timestamps
    end
  end
end
