class AddFeaturedEmployer < ActiveRecord::Migration
  def up
  	add_column :employers, :is_featured, :boolean, :default => false
  end

  def down
  	remove_column :employers, :is_featured
  end
end
