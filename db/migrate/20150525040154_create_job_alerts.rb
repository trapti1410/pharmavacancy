class CreateJobAlerts < ActiveRecord::Migration
  def change
    create_table :job_alerts do |t|
    	t.string :email_id
    	t.integer :candidate_id
    	t.string :keywords
    	t.integer :city_id
    	t.integer :work_experience
    	t.integer :ectc_from
    	t.integer :ectc_to
    	t.integer :category_id
    	t.integer :designation_id
    	t.string :name 

		t.timestamps
    end
  end
end
