class AddFieldsJobalert < ActiveRecord::Migration
  def up
  	add_column :job_alerts, :location, :string
  	add_column :job_alerts, :work_experience_years, :integer
  	add_column :job_alerts, :work_experience_months, :integer
  end

  def down
  	remove_column :job_alerts, :locattion
  	remove_column :job_alerts, :work_experience_years
  	remove_column :job_alerts, :work_experience_months
  end
end
