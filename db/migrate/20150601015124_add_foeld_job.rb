class AddFoeldJob < ActiveRecord::Migration
  def up
  	add_column :jobs, :approved, :boolean, :default => false 
  	add_column :employers, :active, :boolean, :default => true

  	add_column :employments, :category_id, :integer
  	add_column :employments, :therapeutic_segment_id, :integer
  	add_column :desired_jobs, :area1_id, :integer
  	add_column :desired_jobs, :area2_id, :integer
  end

  def down
  	remove_column :jobs, :approved
  	remove_column :employers, :active
  	remove_column :employments, :category_id
  	remove_column :employments, :therapeutic_segment_id
  	remove_column :desired_jobs, :area1_id
  	remove_column :desired_jobs, :area2_id
  end
end
