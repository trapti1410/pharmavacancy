class AddJobField < ActiveRecord::Migration
  def up
  	add_column :jobs, :therapeutic_segment_id, :integer
  end

  def down
  	remove_column :jobs, :therapeutic_segment_id
  end
end
