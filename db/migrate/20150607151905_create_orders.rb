class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.integer :plan_id
      t.string :status
      t.decimal :amount, :precision => 5, :scale => 2

      t.timestamps
    end
  end
end
