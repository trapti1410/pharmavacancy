class AddPricePlan < ActiveRecord::Migration
  def up
  	add_column :plans, :price, :decimal, :precision => 5, :scale => 2
  end

  def down
  	remove_column :plans, :price
  end
end
