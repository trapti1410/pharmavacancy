class AddDefaultColumnPlan < ActiveRecord::Migration
  def up
  	add_column :plans, :is_default, :boolean, :default => false
  end

  def down
  	remove_column :plans, :is_default
  end
end
