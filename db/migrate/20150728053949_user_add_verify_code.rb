class UserAddVerifyCode < ActiveRecord::Migration
  def up
  	add_column :users, :verify_code, :string
  end

  def down
  	remove_column :users, :verify_code
  end
end
