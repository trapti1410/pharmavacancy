# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150728053949) do

  create_table "candidate_draft_jobs", :force => true do |t|
    t.integer  "job_id"
    t.integer  "candidate_id"
    t.boolean  "draft",        :default => true
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.datetime "applied_at"
  end

  create_table "candidate_profile_summaries", :force => true do |t|
    t.text     "summary"
    t.integer  "candidate_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "candidates", :force => true do |t|
    t.string   "email_id"
    t.string   "full_name"
    t.string   "marital_status"
    t.string   "gender"
    t.integer  "dob_day"
    t.integer  "dob_month"
    t.integer  "dob_year"
    t.string   "phone_number"
    t.text     "mailing_address"
    t.integer  "current_location_id"
    t.integer  "current_city_id"
    t.integer  "total_exp_years"
    t.integer  "total_exp_months"
    t.text     "resume_headlines"
    t.integer  "salary_lakhs",        :default => 0
    t.integer  "salary_thousands",    :default => 0
    t.string   "key_skills"
    t.string   "landline_number"
    t.text     "permanent_address"
    t.string   "home_town"
    t.string   "home_town_pincode"
    t.integer  "category_id"
    t.integer  "designation_id"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "notice_period"
    t.integer  "reg_status"
  end

  create_table "candidates_cities", :id => false, :force => true do |t|
    t.integer "candidate_id"
    t.integer "city_id"
  end

  add_index "candidates_cities", ["candidate_id", "city_id"], :name => "index_candidates_cities_on_candidate_id_and_city_id"

  create_table "candidates_jobs", :id => false, :force => true do |t|
    t.integer "candidate_id"
    t.integer "job_id"
  end

  add_index "candidates_jobs", ["candidate_id", "job_id"], :name => "index_candidates_jobs_on_candidate_id_and_job_id"

  create_table "certificates", :force => true do |t|
    t.integer  "candidate_id"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "cities", :force => true do |t|
    t.string   "name"
    t.integer  "location_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "data_files", :force => true do |t|
    t.string   "file_name"
    t.string   "content_type"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "resume_id"
  end

  create_table "designations", :force => true do |t|
    t.string   "name"
    t.string   "short_name"
    t.integer  "vacancy_category_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "desired_jobs", :force => true do |t|
    t.integer  "candidate_id"
    t.boolean  "full_time"
    t.boolean  "part_time"
    t.boolean  "permament"
    t.boolean  "contractual"
    t.integer  "designation_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "expected_sal_lakhs"
    t.integer  "expected_sal_thousands"
    t.integer  "city_id"
    t.integer  "location_id"
    t.text     "change_reason"
    t.text     "therapeutic_segment"
    t.integer  "area1_id"
    t.integer  "area2_id"
  end

  create_table "educations", :force => true do |t|
    t.integer  "candidate_id"
    t.string   "course_type"
    t.integer  "course_id"
    t.integer  "education_type"
    t.string   "specialization"
    t.integer  "year"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "employer_candidate_views", :force => true do |t|
    t.integer  "candidate_id"
    t.integer  "employer_id"
    t.integer  "downloads",    :default => 0
    t.integer  "views",        :default => 0
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "employers", :force => true do |t|
    t.string   "email_id"
    t.string   "company_name"
    t.string   "contact_number"
    t.string   "company_type"
    t.integer  "industry_id"
    t.string   "contact_person"
    t.text     "about_company"
    t.string   "website"
    t.string   "logo_url"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.text     "address"
    t.integer  "location_id"
    t.string   "pincode"
    t.integer  "city_id"
    t.integer  "plan_id",           :default => 1
    t.integer  "profile_views",     :default => 0
    t.integer  "profile_downloads", :default => 0
    t.boolean  "is_featured",       :default => false
    t.boolean  "active",            :default => true
  end

  create_table "employments", :force => true do |t|
    t.integer  "candidate_id"
    t.string   "employer_name"
    t.integer  "employer_type_id"
    t.integer  "start_month"
    t.integer  "start_year"
    t.integer  "end_month"
    t.integer  "end_year"
    t.text     "job_profile"
    t.integer  "designation_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.boolean  "is_present"
    t.integer  "category_id"
    t.integer  "therapeutic_segment_id"
  end

  create_table "industries", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "job_alerts", :force => true do |t|
    t.string   "email_id"
    t.integer  "candidate_id"
    t.string   "keywords"
    t.integer  "city_id"
    t.integer  "work_experience"
    t.integer  "ectc_from"
    t.integer  "ectc_to"
    t.integer  "category_id"
    t.integer  "designation_id"
    t.string   "name"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "location"
    t.integer  "work_experience_years"
    t.integer  "work_experience_months"
  end

  create_table "job_types", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "jobs", :force => true do |t|
    t.string   "title"
    t.integer  "job_type_id"
    t.integer  "no_of_vacancies"
    t.text     "description"
    t.integer  "experience_from"
    t.integer  "experience_to"
    t.integer  "functional_area_id"
    t.integer  "role_id"
    t.integer  "industry_id"
    t.integer  "location_id"
    t.decimal  "ctc_from",                  :precision => 4, :scale => 2
    t.decimal  "ctc_to",                    :precision => 4, :scale => 2
    t.boolean  "ctc_hide",                                                :default => false
    t.date     "expiry_date"
    t.text     "desired_candidate_profile"
    t.integer  "ug_qualification_id"
    t.integer  "pg_qualification_id"
    t.datetime "created_at",                                                                 :null => false
    t.datetime "updated_at",                                                                 :null => false
    t.string   "keywords"
    t.integer  "employer_id"
    t.boolean  "approved",                                                :default => false
    t.integer  "therapeutic_segment_id"
  end

  create_table "jobs_cities", :id => false, :force => true do |t|
    t.integer "job_id"
    t.integer "city_id"
  end

  add_index "jobs_cities", ["job_id", "city_id"], :name => "index_jobs_cities_on_job_id_and_city_id"

  create_table "languages", :force => true do |t|
    t.integer  "candidate_id"
    t.string   "name"
    t.integer  "proficiency_level"
    t.boolean  "read"
    t.boolean  "write"
    t.boolean  "speak"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.string   "short_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "orders", :force => true do |t|
    t.integer  "user_id"
    t.integer  "plan_id"
    t.string   "status"
    t.decimal  "amount",     :precision => 5, :scale => 2
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  create_table "pg_qualifications", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "plans", :force => true do |t|
    t.string   "name"
    t.integer  "view_limit",                                   :default => 0
    t.integer  "download_limit",                               :default => 0
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
    t.decimal  "price",          :precision => 5, :scale => 2
    t.boolean  "is_default",                                   :default => false
  end

  create_table "projects", :force => true do |t|
    t.integer  "candidate_id"
    t.string   "client_name"
    t.string   "title"
    t.integer  "start_month"
    t.integer  "start_year"
    t.integer  "end_month"
    t.integer  "end_year"
    t.text     "details"
    t.text     "role_description"
    t.integer  "team_size"
    t.string   "skills"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "resumes", :force => true do |t|
    t.integer  "candidate_id"
    t.text     "html_format"
    t.string   "url"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "skills", :force => true do |t|
    t.integer  "candidate_id"
    t.string   "name"
    t.integer  "last_used"
    t.integer  "exp_months"
    t.integer  "exp_years"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "ug_qualifications", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "user_name"
    t.string   "password"
    t.string   "role"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "profile_id"
    t.string   "profile_type"
    t.boolean  "verified",     :default => false
    t.boolean  "active",       :default => true
    t.string   "verify_code"
  end

  create_table "vacancy_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
