# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
	if User.where("user_name = ? and role = ?", 'admin@pharmsvacancy.com', 'admin').blank?
		User.create(user_name: 'admin@pharmsvacancy.com', password: 'admin123!', role: 'admin') 
	end

	if User.where("user_name = ? and role = ?", 'admin@pharmsvacancy.com', 'admin').blank?
		User.create(user_name: 'admin@pharmsvacancy.com', password: 'admin123!', role: 'employer')
	end

	if Plan.where("name = ?", 'free').blank?
		Plan.create(name: 'Free', view_limit: 50, download_limit: 50, is_default: true)
	end
