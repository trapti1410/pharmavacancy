namespace :mailtest do
    desc "Rake task to send job alert"
    task :email => :environment do
        c = Candidate.last

        resp = CandidateWelcomeMailer.welcome_email(c).deliver

        puts "Resp: #{resp}"

        puts "#{Time.now} - Success!"

    end
end