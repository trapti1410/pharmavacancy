namespace :job_alert do
    desc "Rake task to send job alert"
    task :send_mail => :environment do
        job_alerts = JobAlert.all

        job_alerts.each do |job_alert|
            jobs = Job.job_alert_search(job_alert)
            if jobs.present?
                status = JobAlertMailer.job_alert(job_alert.email_id, jobs).deliver
                puts "#{Time.now} - Send email to #{job_alert.email_id}!"
            end
        end
        puts "#{Time.now} - Success!"

    end
end